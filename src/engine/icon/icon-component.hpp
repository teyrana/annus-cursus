//Copyright (c) 2023, MIT License

#pragma once

#include "../base-component.hpp"

namespace Engine { 
namespace Icon {

class IconComponent: public BaseComponent<IconComponent> {
public:
    static constexpr aspect_mask_t ASPECT_INDEX = 3;
public:
    IconComponent( int _id ) : BaseComponent(_id) {}
};

} // namespace Icon
} // namespace Engine
