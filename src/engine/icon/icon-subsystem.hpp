//Copyright (c) 2023, MIT License
#pragma once

#include "../aspect-subsystem.hpp"
#include "icon-component.hpp"

namespace Engine {
namespace Icon {


class IconSubsystem: public AspectSubsystem<IconComponent,IconSubsystem> {
public:
    IconSubsystem( /*EntityPool& ep,*/ ComponentTable<IconComponent>& cp )
        : AspectSubsystem( /*ep,*/ cp )
    {}

    void update( const std::chrono::milliseconds& dt );

};


} // namespace Icon
} // namespace Engine
