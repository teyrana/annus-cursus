//Copyright (c) 2023, MIT License

#pragma once

#include <bitset>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>


#include "types.hpp"
#include "entity.hpp"

namespace Engine {

template<typename T> // enforces CRTP
struct AspectComponent {
public:
    AspectComponent( int _eid ) 
        : eid(_eid)
    {}

    AspectComponent( const AspectComponent<T>& other )
        : eid( other.eid )
    {}

    bool dead() const { return reinterpret_cast<T>(this)->dead(); }

    std::string str() const { return reinterpret_cast<T>(this)->str(); }

    aspect_mask_t aspect() const { return T::ASPECT_INDEX; }

public:
    const entity_id_t eid;

protected:
    // ????

};

} // namespace Engine

