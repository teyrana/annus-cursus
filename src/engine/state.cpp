#include <bitset>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

#include "terrain/direction.hpp"
#include "terrain/unit-layer.hpp"

#include "state.hpp"

using Terrain::Direction;
using Terrain::UnitLayer;
using Engine::entity_id_t;
using Engine::State;


State::State( Catalog::Index& _index, Terrain::Map& _map )
    : map_(_map)
    , storage_subsys_( _index )
    , process_subsys_( _index, storage_subsys_ )
    , moveable_subsys_( _index, _map )
    , placeable_subsys_( _index, _map )
{
    current_update_time_ = std::chrono::system_clock::now();
    last_update_time_ = current_update_time_;   
}

entity_id_t State::emplace_building( uint32_t pattern_index, GridCoordinate coord ){
    const entity_id_t eid = entities_.make();
    if( Entity::ERROR_ENTITY_ID == eid )
        return Entity::ERROR_ENTITY_ID;


    component_id_t cid = placeable_subsys_.emplace( eid );

    placeable_subsys_[cid].pattern( pattern_index );

    placeable_subsys_[cid].place( coord );
    placeable_subsys_.place( cid, coord );

    // et al

    return eid;
}


entity_id_t State::emplace_unit( uint32_t pattern_index, GridCoordinate coord ){ 
    const entity_id_t eid = entities_.make();
    if( Entity::ERROR_ENTITY_ID == eid )
        return Entity::ERROR_ENTITY_ID;

    component_id_t cid = moveable_subsys_.emplace( eid );

    moveable_subsys_[cid].pattern( pattern_index );

    moveable_subsys_.place( cid, coord, Direction::NORTHEAST );

    // et al

    return eid;
}


void State::update() {
    current_update_time_ = std::chrono::system_clock::now();
    const auto update_interval = std::chrono::duration_cast<std::chrono::milliseconds>( current_update_time_ - last_update_time_ );

    // ====== Update Subsystems ======
    // Update the system by simply calling it with the current scene

    // movable_updater_.update( update_interval );

    // // nothing to do... yet?
    // placeable_subsys_.update( update_interval );

    storage_subsys_.update( update_interval );

    // update storage _after_ processes
    storage_subsys_.update( update_interval );


    // ====== Clean up, for next iteration ======
    last_update_time_ = current_update_time_;

}
