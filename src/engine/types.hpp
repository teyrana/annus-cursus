//Copyright (c) 2023, MIT License

#pragma once

#include <bitset>
#include <cstdint>

namespace Engine {

using entity_id_t = uint32_t;

using component_id_t = uint32_t;

using aspect_mask_t = uint32_t;
using aspect_index_t = uint8_t;
const int MAX_ASPECT_COUNT = sizeof(uint32_t);
static_assert( MAX_ASPECT_COUNT == sizeof(aspect_mask_t) );

// assign which bit flag indicates which subsystem, here
enum ASPECT_FLAG {
    NOOP = 0,
    ICON,
    MOVABLE,
    PLACEABLE,
    PROCESS,
    STORAGE
};


} // namespace Engine

