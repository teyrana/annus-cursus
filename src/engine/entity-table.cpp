// Copyright (c) 2023, MIT License

#include <map>

#include "entity-table.hpp"

using Engine::entity_id_t;
using Engine::Entity;
using Engine::EntityTable;

entity_id_t EntityTable::first() const { 
    return 0;
}

entity_id_t EntityTable::last() const { 
    return table_.size();
}

Entity* EntityTable::get( entity_id_t eid ){
    if( eid < table_.size() ){
        return const_cast<Entity*>(&table_.at(eid));
    }else{
        return nullptr;
    }
}

entity_id_t EntityTable::make() { 
    entity_id_t new_id = table_.size();
    table_.emplace_back( new_id );
    return new_id;
}

entity_id_t EntityTable::make( entity_id_t eid ) { 
    if( eid < table_.size() ){
        return eid;
    }else{
        return make();
    }
}

entity_id_t EntityTable::next( entity_id_t i ) const{
    return ++i;
}

size_t EntityTable::size() const {
    return table_.size();
}

