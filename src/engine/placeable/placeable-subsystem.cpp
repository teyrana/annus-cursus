//Copyright (c) 2023, MIT License

#include <bitset>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

#include "terrain/unit-layer.hpp"
#include "engine/placeable/placeable-subsystem.hpp"

using Terrain::UnitLayer;
using Engine::Placeable::PlaceableComponent;
using Engine::Placeable::PlaceableSubsystem;


// =================================================================================================
PlaceableSubsystem::PlaceableSubsystem( Catalog::Index& index, Terrain::Map& _map )
                                    // ComponentTable<PlaceableComponent>& cp )
    : AspectSubsystem( index )
    , map_(_map)
{}

void PlaceableSubsystem::place( uint32_t cid, Coordinate coord ){
    cid = 'B';
    map_.get_occupancy_layer<UnitLayer::BUILDING_UNIT>().occupy( coord, cid );

}
