//Copyright (c) 2023, MIT License

#pragma once

#include "terrain/map.hpp"
#include "terrain/coordinate.hpp"
#include "engine/aspect-subsystem.hpp"
#include "engine/component-table.hpp"
#include "engine/process/process-subsystem.hpp"
#include "engine/placeable/placeable-component.hpp"

using Coordinate = Terrain::GridCoordinate;

namespace Engine {
namespace Placeable {

class PlaceableSubsystem:
    public AspectSubsystem<PlaceableSubsystem>,
    public ComponentTable<PlaceableComponent> 
{
public:
    PlaceableSubsystem() = delete;

    PlaceableSubsystem( Catalog::Index& index, Terrain::Map& _map );

    void place( uint32_t placeable_component_id, Coordinate coord );

    void update( std::chrono::milliseconds /*dt*/ ) {}


    // Process::ProcessSubsystem& process_subsys_;

private:
    Terrain::Map& map_;

};

} // namespace Movable
} // namespace Engine