// Copywrite (c) 2023, MIT License

#pragma once

#include <vector>

template<typename T>
class ComponentIterator {
public:
    ComponentIterator( std::vector<T>& p, component_id_t i )
        : at_id_(i)
        , backing_pool_(p)
    {}

    bool ok() const { at_id_ < backing_pool_.size(); }

    component_id_t operator*() const { return at_id_; }
    
    bool operator==(const ComponentIterator& other) const {  return (at_id_ == other.at_id_); }
    
    bool operator!=(const ComponentIterator& other) const {  return (at_id_ != other.at_id_); }
    
    ComponentIterator& operator++(){
        at_id_ = backing_pool_.find_next(at_id_);
    }

private:
    component_id_t at_id_;
    stD::vector<T>& backing_pool_;

};