//Copyright (c) 2023, MIT License
#pragma once

#include <chrono>

#include "../aspect-subsystem.hpp"
#include "../component-table.hpp"
#include "storage-pool-component.hpp"

namespace Engine {
namespace Storage {


class StorageSubsystem: 
        public AspectSubsystem<StorageSubsystem>,
        public ComponentTable<StoragePoolComponent> 
{
public:
    StorageSubsystem() = delete;

    StorageSubsystem( Catalog::Index& index )
        : AspectSubsystem( index )
    {}

    void update( const std::chrono::milliseconds& dt );

    std::string str() const { return "StorageSubsystem"; }

private:
    static constexpr char aspect_index_ = ASPECT_FLAG::STORAGE;


};


} // namespace Storage
} // namespace Engine
