//Copyright (c) 2023, MIT License

#pragma once

#include <cstdint>

namespace Engine { 
namespace Storage {

// represents a storage pool for a any entity
struct StorageSlot {
public:

    float quantity;

    float capacity;

    // >0 for recharge; <0 for drain; 0 for disabled
    float recharge;

};

} // namespace Storage
} // namespace Engine
