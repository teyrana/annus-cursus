//Copyright (c) 2023, MIT License

#include "storage-pool-component.hpp"
#include "storage-subsystem.hpp"

using Engine::Storage::StoragePoolComponent;
using Engine::Storage::StorageSubsystem;

void StorageSubsystem::update( const std::chrono::milliseconds& /*dt*/ ){

    for( auto& each_storage_pool_ : table_ ){
        for( auto& [id,slot] : each_storage_pool_.slots_ ){
            if( (0 != slot.recharge) ){
                slot.quantity += slot.recharge;
            }
            if( slot.quantity > slot.capacity ){
                slot.quantity = slot.capacity;
            }
        }
    }

};


