//Copyright (c) 2023, MIT License

#pragma once

#include <vector>

#include "../aspect-component.hpp"
#include "../types.hpp"
#include "storage-slot.hpp"


namespace Engine { 
namespace Storage {

// represents a storage pool for a any entity
class StoragePoolComponent: public AspectComponent<StoragePoolComponent> {
public:
    StoragePoolComponent( int _id ) : 
        AspectComponent(_id)
    {}

    void adjust( int rid, float qty ) {
        for( auto& slot_entry : slots_ ){
            if( slot_entry.first == rid ){
                slot_entry.second.quantity += qty;
                return;
            }
        }
        // if we get here, we didn't find the resource in the pool

        // check if we have storage space available:
        // if( available( rid ) ){
        // slots_[rid] = { qty, -1, 0 };
        // }
    }

    bool contains( int rid, float qty ) const { 
        if( slots_.contains(rid) && (slots_.at(rid).quantity >= qty) ){
            return true;
        }
        return false;
    }

    const StorageSlot& emplace( int rid, float qty, float cap, float rec ) {
        return slots_[rid] = { qty, cap, rec };
    }

    StorageSlot& operator[]( size_t idx ) { return slots_[idx]; }

    size_t size() const { return slots_.size(); }

friend class StorageSubsystem;
private:
    std::map<int, StorageSlot> slots_;

};

} // namespace Storage
} // namespace Engine
