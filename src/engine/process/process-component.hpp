//Copyright (c) 2023, MIT License

#pragma once

#include "engine/aspect-component.hpp"
#include "engine/types.hpp"

namespace Engine { 
namespace Process {

class ProcessComponent: public AspectComponent<ProcessComponent> {
public:
    ProcessComponent( int _id )
        : AspectComponent(_id)
        , process_id_(_id)
        , throttle_percent_(0)
    {}

    int id() const { return process_id_; }
    
    int pool() const { return pool_id_; }
    
    uint8_t throttle() const { return throttle_percent_; }

public:
    int process_id_;
    int pool_id_;
    uint8_t throttle_percent_;


};

} // namespace Process
} // namespace Engine
