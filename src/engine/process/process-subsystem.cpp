//Copyright (c) 2023, MIT License

#include "catalog/index.hpp"
#include "process-component.hpp"
#include "process-subsystem.hpp"
#include "engine/storage/storage-subsystem.hpp"

using Catalog::ProcessTypeEntry;
using Engine::Process::ProcessComponent;
using Engine::Process::ProcessSubsystem;
using Engine::Storage::StoragePoolComponent;


ProcessSubsystem::ProcessSubsystem( Catalog::Index& index, Storage::StorageSubsystem& storage )
    : AspectSubsystem(index)
    , storage_subsys_(storage)
{}


// note: explicitly _not_ a ProcessSubsystem:: member function, mostly for privacy
void tick( const Catalog::Index& index, ProcessComponent& proc, StoragePoolComponent& pool, std::chrono::milliseconds dt ){
    bool pool_has_inputs = false;

    const auto& proc_entry = index.process[ proc.id() ];

    for( const auto& entry : proc_entry.io() ){
        const auto& resource_name = entry.first;
        // hallucination.  // need to implement this, or something similar
        const auto& resource_id = index.resource.lookup(resource_name);
        const auto& quantity = entry.second;
        if( (0 < quantity) && pool.contains( resource_id, quantity) )
            pool_has_inputs = true;
    }

    // processes are defined per-second -- i.e. if a process generates 1u per second, and it gets 
    // updated every 100ms, it will generate 0.1u every update.
    const float scale = proc.throttle() * static_cast<float>(dt.count()) / 1000.0f;

    if( pool_has_inputs ){
        for( const auto& entry : proc_entry.io() ){
            const auto& resource_name = entry.first;
            const auto& resource_id = index.resource.lookup(resource_name);
            const auto& qty = scale * entry.second; //note: may be positive or negative

            pool.adjust( resource_id, qty ); // TODO: test +/- qty effect
        }
    }
}

void ProcessSubsystem::update( std::chrono::milliseconds dt ){
    for( auto& each_process : table_ ){
        if( 0 < each_process.throttle() ){
            auto& pool = storage_subsys_[ each_process.pool() ];
            std::cout << "ProcessSubsystem::update() - throttle: " << each_process.throttle() << " % " << std::endl;
            tick( index_, each_process, pool, dt );
        }
    }
};


