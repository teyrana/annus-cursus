//Copyright (c) 2023, MIT License
#pragma once

#include <chrono>

#include "engine/aspect-subsystem.hpp"
#include "engine/component-table.hpp"
#include "engine/storage/storage-subsystem.hpp"
#include "engine/process/process-component.hpp"

namespace Engine {
namespace Process {

class ProcessSubsystem:
    public AspectSubsystem<ProcessSubsystem>,
    public ComponentTable<ProcessComponent> 
{
public:
    ProcessSubsystem() = delete;

    ProcessSubsystem( Catalog::Index & index, Storage::StorageSubsystem& storage );

    std::string str() const { return "ProcessSubsystem"; }

    void update( std::chrono::milliseconds dt );


private:
    static constexpr char aspect_index_ = ASPECT_FLAG::PROCESS;


private:
    Storage::StorageSubsystem& storage_subsys_;


};


} // namespace Process
} // namespace Engine
