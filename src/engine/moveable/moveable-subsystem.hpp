//Copyright (c) 2023, MIT License

#pragma once

#include "terrain/direction.hpp"
#include "terrain/map.hpp"
#include "terrain/coordinate.hpp"

#include "engine/aspect-subsystem.hpp"
#include "engine/component-table.hpp"
#include "engine/moveable/moveable-component.hpp"
#include "engine/process/process-subsystem.hpp"

using Terrain::Direction;
using Coordinate = Terrain::GridCoordinate;

namespace Engine {
namespace Moveable {

class MoveableSubsystem:
    public AspectSubsystem<MoveableSubsystem>,
    public ComponentTable<MoveableComponent> 
{
public:
    MoveableSubsystem() = delete;

    MoveableSubsystem( Catalog::Index& index, Terrain::Map& _map );

    // incremental movement
    void move( uint32_t placeable_component_id, Coordinate coord, Direction dir );

    // initial placement
    void place( uint32_t placeable_component_id, Coordinate coord, Direction dir );

    void update( std::chrono::milliseconds /*dt*/ ) {}

    // Process::ProcessSubsystem& process_subsys_;

private:
    Terrain::Map& map_;

};

} // namespace Movable
} // namespace Engine