//Copyright (c) 2023, MIT License

#include <bitset>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

#include "terrain/unit-layer.hpp"
#include "engine/moveable/moveable-subsystem.hpp"

using Terrain::UnitLayer;
using Engine::Moveable::MoveableComponent;
using Engine::Moveable::MoveableSubsystem;


// =================================================================================================
MoveableSubsystem::MoveableSubsystem( Catalog::Index& index, Terrain::Map& _map )
                                    // ComponentTable<MoveableComponent>& cp )
    : AspectSubsystem( index )
    , map_(_map)
{}

void MoveableSubsystem::move( uint32_t cid, Coordinate coord, Direction /*dir*/ ){
    // NYI: check movement
    map_.get_occupancy_layer<UnitLayer::GROUND_UNIT>().occupy( coord, cid );
}

void MoveableSubsystem::place( uint32_t cid, Coordinate coord, Direction /*dir*/ ){
    cid = 'M';
    map_.get_occupancy_layer<UnitLayer::GROUND_UNIT>().occupy( coord, cid );
}
