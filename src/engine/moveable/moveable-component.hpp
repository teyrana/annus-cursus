//Copyright (c) 2023, MIT License

#pragma once

#include <cfloat>

#include "terrain/coordinate.hpp"

#include "engine/aspect-component.hpp"

using Terrain::GridCoordinate;

namespace Engine {
namespace Moveable {

class MoveableComponent : public AspectComponent<MoveableComponent> {
public:
    MoveableComponent( int _id )
        : AspectComponent(_id)
        , pattern_index_(0)
        , position_({0,0})
    {}

    std::pair<uint32_t,uint32_t> at() const { 
        return position_; }

    void move( GridCoordinate& at ){ 
        position_ = at; }

    uint32_t pattern() const { 
        return pattern_index_; }

    void pattern( uint32_t _pattern_index ){
        pattern_index_ = _pattern_index; }


    // placeholder implementation
    std::string str() const {
        return "MoveableComponent"; }

private:
    uint32_t pattern_index_;

    GridCoordinate position_;

};

} // namespace Movable

} // namespace Engine
