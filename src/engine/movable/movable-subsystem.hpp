//Copyright (c) 2023, MIT License

#pragma once

#include "../aspect-subsystem.hpp"

#include "movable-component.hpp"

namespace Engine {
namespace Movable {

class MovableSubsystem: public AspectSubsystem<MovableComponent,MovableSubsystem> {
public:
    MovableSubsystem( /*EntityPool& ep,*/ ComponentTable<MovableComponent>& cp )
        : AspectSubsystem( /*ep,*/ cp )
    {}

    void update( const std::chrono::milliseconds& dt );

};

} // namespace Movable
} // namespace Engine