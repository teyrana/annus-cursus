//Copyright (c) 2023, MIT License

#pragma once

#include <cfloat>

#include "../base-component.hpp"

namespace Engine {
namespace Movable {

class MovableComponent : public BaseComponent<MovableComponent> {
public:
    static constexpr aspect_mask_t ASPECT_INDEX = 4;
public:
    MovableComponent( int _id )
        : BaseComponent(_id)
        , platform_(nullptr)
        , engine_(nullptr)
        , mass_kg(std::numeric_limits<float>::quiet_NaN())
        , fuel_kg(std::numeric_limits<float>::quiet_NaN())
    {}

    // placeholder implementation
    bool dead() const { return true; } 

    // placeholder implementation
    std::string str() const { return "MovableComponent"; }

private:
    // placeholder
    // todo: reference platform class instance from catalog
    void* platform_;
    
    // placeholder
    // todo: reference engine class instance from catalog
    void* engine_;

    float mass_kg;
    
    float fuel_kg;

};

} // namespace Movable

} // namespace Engine
