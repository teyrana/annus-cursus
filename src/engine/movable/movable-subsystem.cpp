//Copyright (c) 2023, MIT License

#include <bitset>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

#include "movable-subsystem.hpp"

using Engine::Movable::MovableComponent;
using Engine::Movable::MovableSubsystem;


void MovableSubsystem::update( const std::chrono::milliseconds& dt ){
    auto* vp0 = components_.get(0);

    // Loop over the entities you're interested in
    std::cout << "    ::Update Movable Subsystem\n";
    std::cout << "    ::[" << ((void*)vp0) << ", ...]\n";
    std::cout << "    =======================\n";
    for( auto& each : components_ ) {
        // DEBUG
        std::cout << "    ::@_" << ((void*)&each) << "_\n";

        // NYI // Do stuff

    }
}

