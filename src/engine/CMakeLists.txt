# Copyright (C) 2023, MIT License

message("    >> Adding Engine Library")

SET( LIB_NAME engine )

SET( LIB_SOURCES
    entity-table.cpp
    # icon/icon-subsystem.cpp
    moveable/moveable-subsystem.cpp
    placeable/placeable-subsystem.cpp
    process/process-subsystem.cpp
    storage/storage-subsystem.cpp
    state.cpp
)

SET( LIB_INCLUDES
    aspect-component.hpp
    aspect-subsystem.hpp
    component-table.hpp
    entity.hpp
    entity-table.hpp
    # icon/icon-component.hpp
    # icon/icon-subsystem.hpp
    moveable/moveable-component.hpp
    moveable/moveable-subsystem.hpp
    placeable/placeable-component.hpp
    placeable/placeable-subsystem.hpp
    process/process-component.hpp
    process/process-subsystem.hpp
    storage/storage-pool-component.hpp
    storage/storage-slot.hpp
    storage/storage-subsystem.hpp
    state.hpp
    types.hpp
)

# full library
ADD_LIBRARY( ${LIB_NAME} ${LIB_SOURCES} ${LIB_INCLUDES} )

# TARGET_INCLUDE_DIRECTORIES( ${LIB_NAME}
#     PUBLIC ${CMAKE_SOURCE_DIR}/src/app/pMOOSDDSBridge/
# )

# TARGET_LINK_LIBRARIES( ${LIB_NAME}
#     ${FASTDDS_LIBRARIES}
#     moos-core-autogen-dds
#     ${MOOS_IVP_LIBRARIES}
#     # ${PROJ_LIBRARY}
#     ${SYSTEM_LIBS} )


# ================================================================
# # DEBUG / DEVEL / LEARNING
# message("::DEBUG::${LIB_NAME}:: INCLUDE_DIRECTORIES")
# get_target_property( DEBUG_PATHS ${LIB_NAME} INTERFACE_INCLUDE_DIRECTORIES )
# foreach( EACH_DIR  ${DEBUG_PATHS} )
#         message("      ::${EACH_DIR}")
# endforeach()
#
# message("::DEBUG::${LIB_NAME}:: LINK_LIBRARIES")
# get_target_property( DEBUG_LIBRARIES ${LIB_NAME} LINK_LIBRARIES )
# foreach( EACH_LIB  ${DEBUG_LIBRARIES} )
#         message("      ::${EACH_LIB}")
# endforeach()
