// Copyright (c) 2023, MIT License

#pragma once

#include <vector>

#include "types.hpp"
#include "entity.hpp"

namespace Engine {

// class EntityTable::EntityIterator;

class EntityTable {
public: 
    using iter_t = std::vector<Entity>::iterator;
public:
    EntityTable() = default;
    ~EntityTable() = default;

    iter_t begin(){ return table_.begin(); }

    iter_t end(){ return table_.end(); }

    entity_id_t first() const;

    Entity* get( entity_id_t eid );
    
    entity_id_t make();
    entity_id_t make( entity_id_t eid );

    entity_id_t next( entity_id_t i = 0 ) const;

    entity_id_t last() const;

    size_t size() const;

private:
    std::vector<Entity> table_;

};

} // namespace Engine
