//Copyright (c) 2023, MIT License

#pragma once

#include <bitset>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>



#include "../catalog/index.hpp"
#include "types.hpp"
#include "entity.hpp"


namespace Engine {

template<typename subsystem_t> // enforces CRTP
struct AspectSubsystem {
public:
    AspectSubsystem() = delete;

    AspectSubsystem( Catalog::Index& index ): 
        index_(index)
    {}

    std::string str() const { return reinterpret_cast<subsystem_t>(this)->str(); }

    aspect_mask_t aspect() const { return subsystem_t::ASPECT_INDEX; }

    // bool link( entity_id_t id ){
    //     return static_cast<subsystem_t*>(this)->link(id); }

    void update( std::chrono::milliseconds dt ) {
        static_cast<subsystem_t*>(this)->update(dt); }

protected:
    // udpate values are normalized to be updated at this interval -- 
    // -> something that changes 1u every 100ms will change/move half as much if updated every 50ms;
    constexpr static std::chrono::seconds nominal_update_interval_ {100};

    const Catalog::Index& index_;

};

} // namespace Engine

