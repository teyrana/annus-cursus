//Copyright (c) 2023, MIT License

#pragma once

#include "types.hpp"
#include "entity.hpp"
// #include "component-iterator.hpp"

namespace Engine {

template<typename component_t>
class ComponentTable {
public:
    using iter_t = std::vector<component_t>::iterator;

public:
    ComponentTable() = default;
    ~ComponentTable() = default;

    /// "safe"(er) version
    /// \return valid component pointer, if it exists; else nullptr.
    component_t* get( component_id_t cid ) {
        return ( cid < table_.size() )
                ? (&table_.at(cid))
                : nullptr; }

    /// \warning this version may through if you screw up the id / indexing 
    /// \return reference to the component; else raise an exception.
    component_t& operator[]( component_id_t cid ){ return ( table_[cid] ); }

    iter_t begin(){ return table_.begin(); }

    iter_t end(){ return table_.end(); }

    component_id_t emplace( entity_id_t eid ){
        table_.emplace_back( eid );
        return ( table_.size() - 1 );
    }

protected:
    // holds individual components, which may or may not contain backing shared objects 
    // (it is expected (but not enforced) that the components use a flyweight pattern)
    std::vector<component_t> table_;

};


} // namespace Engine
