//Copyright (c) 2023, MIT License

#pragma once

#include <chrono>
#include <memory>
#include <stack>

#include "../catalog/index.hpp"
#include "entity-table.hpp"
#include "component-table.hpp"
// #include "icon/icon-subsystem.hpp"
#include "moveable/moveable-subsystem.hpp"
#include "placeable/placeable-subsystem.hpp"
#include "process/process-subsystem.hpp"
#include "storage/storage-subsystem.hpp"
// #include "../terrain/coordinate.hpp"
#include "../terrain/map.hpp"
#include "types.hpp"

using Terrain::GridCoordinate;

namespace Engine {

using Placeable::PlaceableComponent;
using Process::ProcessComponent;
using Storage::StoragePoolComponent;

// ====== ======
// See Also: 
// - https://t-machine.org/index.php/2008/03/13/entity-systems-are-the-future-of-mmos-part-4/
// - https://t-machine.org/index.php/2009/10/26/entity-systems-are-the-future-of-mmos-part-5/
class State {
public:
    State() = delete;

    State(Catalog::Index& index, Terrain::Map& map );

    ~State() = default;

    size_t entity_count() const { return entities_.size(); }

    // template<typename T>
    // T* assign( entity_id_t id );

    void update();

public: // Emplacers
// ============================
    entity_id_t emplace_building( uint32_t entry_index, GridCoordinate coord );

    entity_id_t emplace_unit( uint32_t entry_index, GridCoordinate coord );


public: // Getters
// ============================
    StoragePoolComponent* storage( uint32_t i ) { return storage_subsys_.get(i); }

    ProcessComponent* process( uint32_t i ) { return process_subsys_.get(i); }

    PlaceableComponent* placeable( uint32_t i ) { return placeable_subsys_.get(i); }

// External References
// ============================
private:
    Terrain::Map& map_;

// Subsystems
// ============================
private:
    Engine::EntityTable entities_;

    // initialized in dependency-order:
    Storage::StorageSubsystem storage_subsys_;
    Process::ProcessSubsystem process_subsys_;
    Engine::Moveable::MoveableSubsystem moveable_subsys_;
    Placeable::PlaceableSubsystem placeable_subsys_;

    // Engine::ComponentTable<MovableComponent> movables_;

//     // link this entity to an immutable asset:
//     template<typename T>
//     component_id_t link( entity_id_t eid, component_id_t cid );


// Internal, Ephemeral State
// ============================
private:
    using time_point_t = std::chrono::time_point<std::chrono::system_clock>;
    time_point_t last_update_time_;
    time_point_t current_update_time_;

};


} // namespace Engine
