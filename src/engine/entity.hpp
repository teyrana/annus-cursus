// Copyright (c) 2023, MIT License

#pragma once

#include <bitset>
#include <cstdint>
#include <limits>

#include "types.hpp"

namespace Engine {

struct Entity {
public:
    static constexpr entity_id_t ERROR_ENTITY_ID = std::numeric_limits<entity_id_t>::max();

public:
    entity_id_t id_;
    std::bitset<MAX_ASPECT_COUNT> mask;

    bool used() const { return mask.any(); }

public:
    Entity( entity_id_t _id ): id_(_id) {}
};

} // namespace Engine
