//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <memory>
#include <stack>

#include "height-layer.hpp"
#include "unit-layer.hpp"

namespace Terrain {

class Map {
public:
    using height_t = float;
    using height_layer_t = HeightLayer<height_t>;

public: 
    Map();

    Map( const std::string& _name, uint32_t _size_i, uint32_t _size_j );

    ~Map() = default;

    void size( uint32_t _width, uint32_t _height );

    std::string str() const;

// ====== ====== Generation Functions ====== ====== 
public:
   height_layer_t& emplace_height_layer( material_t mat );

// ====== ====== Accessors ====== ======
public:
    float cell_width() const { return cell_width_m; }
    float scale_factor() const { return cell_width_m; }

    explicit operator bool() const { return 0 < height_layers_.size(); }

    uint32_t count_height_layers() const { return height_layers_.size(); }

    height_layer_t& get_height_layer_by_index( uint32_t index ){ return *height_layers_[index]; }

    height_layer_t& get_surface_layer(){ return *height_layers_[ height_layers_.size() - 1 ]; }

    template<UnitLayer::UNIT_TYPE unit_t>
    UnitLayer& get_occupancy_layer();
    // UnitLayer& get_occupancy_layer( UnitLayer::UNIT_TYPE unit );

    uint32_t size() const { return sizes_.first * sizes_.second; }
    uint32_t size_i() const { return sizes_.first; }
    uint32_t size_j() const { return sizes_.second; }


// ====== ====== Properties Functions ====== ====== 
public:
    const std::string name;

private:
    // \brief number of cells across the grid, in each of x & y directions
    std::pair<uint32_t, uint32_t> sizes_;

    std::vector< std::unique_ptr<height_layer_t> > height_layers_;

    std::unique_ptr<UnitLayer> ground_unit_layer_;
    std::unique_ptr<UnitLayer> air_unit_layer_;
    std::unique_ptr<UnitLayer> submarine_unit_layer_;
    std::unique_ptr<UnitLayer> underground_unit_layer_;


    /// \brief how many meters wide each cell is, in physical units (meters)
    const float cell_width_m = 10.f;

};


} // namespace Terrain
