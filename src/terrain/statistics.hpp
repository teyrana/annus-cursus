// Copyright (C) 2022, MIT License, Daniel Williams
#pragma once



namespace Terrain {



struct Statistics {
public:
    // statistical measures
    float min;
    float max;
    float mean;
    float std_dev;
    float range;

    Statistics()
        : min(0)
        , max(0)
        , mean(0)
        , std_dev(0)
        , range(0)
    {}

};


}   // namespace Terrain
