//Copyright (c) 2023, MIT License

#pragma once

namespace Terrain {

// define, starting with North and rotate clockwise
enum class Direction {
    NORTH = 0,
    NORTHEAST = 45,
    EAST = 90,
    SOUTHEAST = 135,
    SOUTH = 180,
    SOUTHWEST = 225,
    WEST = 270,
    NORTHWEST = 315
};

}  // namespace Terrain
