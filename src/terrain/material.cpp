//Copyright (c) 2022, MIT License, Daniel Williams

#include <string>

#include "material.hpp"

Terrain::material_t Terrain::parse_material( const std::string& name ){ 
    if( "rock" == name ){
        return MATERIAL_ROCK;
    }else if( "bedrock" == name ){
        return MATERIAL_ROCK_BEDROCK;
    }else if( "ore" == name ){
        return MATERIAL_ROCK_ORE;
    }else if( "bauxite" == name ){
        return MATERIAL_ROCK_ORE_BAUXITE;
    }else if( "concrete" == name ){
        return MATERIAL_CONCRETE;
    }else if( "rubble" == name ){
        return MATERIAL_RUBBLE;
    }else if( "sand" == name ){
        return MATERIAL_SAND;
    }else if( "soil" == name ){
        return MATERIAL_SOIL;
    }else if( "water" == name ){
        return MATERIAL_WATER;
    }else if( "fresh-water" == name ){
        return MATERIAL_WATER_FRESH;
    }else if( "salt-water" == name ){
        return MATERIAL_WATER_SALT;
    }else{
        return MATERIAL_SOIL;
    }
}

std::string describe_material( Terrain::material_t mat ){
    if( Terrain::MATERIAL_ROCK == mat ){
        return "rock";
    }else if( Terrain::MATERIAL_ROCK_BEDROCK == mat){
        return "bedrock";
    }else if( Terrain::MATERIAL_ROCK_ORE == mat ){
        return "ore";
    }else if( Terrain::MATERIAL_ROCK_ORE_BAUXITE == mat){
        return "bauxite";
    }else if( Terrain::MATERIAL_CONCRETE == mat){
        return "concrete";
    }else if( Terrain::MATERIAL_RUBBLE == mat){
        return "rubble";
    }else if( Terrain::MATERIAL_SAND == mat){
        return "sand";
    }else if( Terrain::MATERIAL_SOIL == mat){
        return "soil";
    }else if( Terrain::MATERIAL_WATER == mat){
        return "water";
    }else if( Terrain::MATERIAL_WATER_FRESH == mat){
        return "fresh-water";
    }else if( Terrain::MATERIAL_WATER_SALT == mat){
        return "salt-water";
    }else{
        return "Unknown?";
    }
}
