//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <memory>
#include <stack>
#include <string>
#include <sstream>

#include "height-layer.hpp"
#include "map.hpp"

using Terrain::HeightLayer;
using Terrain::Map;
using Terrain::UnitLayer;

Map::Map()
    : Map("default_map",16,16)
{}

Map::Map( const std::string& _name, uint32_t _size_i, uint32_t _size_j )
    : name(_name)
    , sizes_(_size_i,_size_j)
{

    air_unit_layer_ = std::make_unique<UnitLayer>(UnitLayer::AIR_UNIT, sizes_ );
    ground_unit_layer_ = std::make_unique<UnitLayer>(UnitLayer::GROUND_UNIT, sizes_ );
    submarine_unit_layer_ = std::make_unique<UnitLayer>(UnitLayer::SUBMARINE_UNIT, sizes_ );
    underground_unit_layer_ = std::make_unique<UnitLayer>(UnitLayer::SUBTERRAIN_UNIT, sizes_ );

}

template<>
UnitLayer& Map::get_occupancy_layer<UnitLayer::AIR_UNIT>() { 
    return *air_unit_layer_;
}

template<>
UnitLayer& Map::get_occupancy_layer<UnitLayer::BUILDING_UNIT>() { 
    return *ground_unit_layer_;
}

template<>
UnitLayer& Map::get_occupancy_layer<UnitLayer::GROUND_UNIT>() { 
    return *ground_unit_layer_;
}

template<>
UnitLayer& Map::get_occupancy_layer<UnitLayer::SUBMARINE_UNIT>(){
    return *submarine_unit_layer_;
}

template<>
UnitLayer& Map::get_occupancy_layer<UnitLayer::SUBTERRAIN_UNIT>(){
    return *underground_unit_layer_;
}

// UnitLayer& Map::get_occupancy_layer( UnitLayer::UNIT_TYPE unit ) {
//     switch( unit ){
//         default:
//         case UnitLayer::AIR_UNIT:
//             return *air_unit_layer_;
//         case UnitLayer::GROUND_UNIT:
//             return *ground_unit_layer_;
//         case UnitLayer::SUBMARINE_UNIT:
//             return *submarine_unit_layer_;
//         case UnitLayer::SUBTERRAIN_UNIT:
//             return *underground_unit_layer_;
//     }
// }

void Map::size( uint32_t _width, uint32_t _height ){
    sizes_ = { _width, _height};
}

std::string Map::str() const {
    // std::ostringstream ss;
    // ss << get_unit_layer(UnitLayer::GROUND_UNIT).str();
    // return ss.str();
    return const_cast<Map*>(this)->get_occupancy_layer<UnitLayer::GROUND_UNIT>().str();
}

HeightLayer<float>& Map::emplace_height_layer( material_t mat ){
    const uint32_t index = height_layers_.size();
    height_layers_.emplace_back( std::make_unique<height_layer_t>( size_i(), size_j(), cell_width_m, mat) );
    return get_height_layer_by_index(index);
}

