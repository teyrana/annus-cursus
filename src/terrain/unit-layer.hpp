//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <algorithm>
#include <array>
#include <cstring>
#include <vector>
// #include <utility>

#include "coordinate.hpp"
#include "material.hpp"

namespace Terrain { 


class UnitLayer {
public:
    // \brief All cells in this layer of this type. Use this type for accessing/consuming this layer
    using cell_t = uint32_t;

    enum UNIT_TYPE {
        AIR_UNIT = 0,
        BUILDING_UNIT = 1,
        GROUND_UNIT = 2,
        SUBMARINE_UNIT = 3,
        SUBTERRAIN_UNIT = 4,
        UNKNOWN_UNIT = 5
    };
    static const std::string layer_type_names[5];

public:
    UnitLayer( UNIT_TYPE unit_layer_type, std::pair<uint32_t,uint32_t> _sizes );

    ~UnitLayer() = default;

    /// \brief check if the given grid coordinate falls within the boundary of this terrain
    ///
    // \warning this test relies on `static_cast<uint8_t>(-1)` wrapping around to a very high number for unsigned integers
    // \warning -- this is platform dependent behavior! (2s-complement representation) --tested on x86_64 + gcc12 + c++23
    float contains( const GridCoordinate& c ) const {
        return ( (c.first < size_i()) && (c.second < size_j()) );
    }

    // this function may copy and/or transform its data...
    // -- so it needs to handle ownership -> std::vector
    template<typename as_t>
    inline std::vector<as_t> data() const;

    inline cell_t& occupied( uint32_t i ) {
        return occupied_[i];
    }

    inline cell_t occupied( uint32_t i ) const {
        return occupied_[i];
    }

    cell_t& occupy( const GridCoordinate& c, cell_t value ) {
        const auto i = index(c);
        occupied_[i] = value;
        return occupied_[i];
    }

    cell_t occupied( const GridCoordinate& c ) const {
        return occupied_[ index( c ) ];
    }

    inline uint32_t index( const GridCoordinate& c ) const { 
        return c.first + c.second * sizes_.first; }

    std::string name() const;

    const std::string& get_layer_type() const;

    std::string str() const;

    uint32_t size() const { return occupied_.size(); }
    uint32_t size_i() const { return sizes_.first; }
    uint32_t size_j() const { return sizes_.second; }

// ====== ====== External Functions ====== ====== 

private:

    const UNIT_TYPE layer_type_;

    const std::pair<uint32_t,uint32_t> sizes_;

private:
    std::vector<cell_t> occupied_;

};


}   // namespace Terrain
