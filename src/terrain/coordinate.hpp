//Copyright (c) 2023, MIT License, Daniel Williams

#pragma once

#include <cmath>
#include <cstdint>
#include <utility>

namespace Terrain {

using GridCoordinate=std::pair<uint32_t,uint32_t>;

inline float norm2( const GridCoordinate& a, const GridCoordinate& b ) {
    return (  std::pow((static_cast<int32_t>(a.first) - static_cast<int32_t>(b.first)), 2)
            + std::pow((static_cast<int32_t>(a.second) - static_cast<int32_t>(b.second)), 2));
}

inline float norm( const GridCoordinate& a, const GridCoordinate& b ) {
    return std::sqrt( norm2( a, b ) );
}


};  // namespace Terrain