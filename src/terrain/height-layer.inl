
// this file is included inside the "Terrain" namespace:
// namespace Terrain {

template<typename cell_t>
std::pair<cell_t, cell_t> HeightLayer<cell_t>::get_min_max() const {
    cell_t min_value = std::numeric_limits<cell_t>::max();
    cell_t max_value = std::numeric_limits<cell_t>::min();

    for( const auto value : heights_ ){
        min_value = std::min( min_value, value );
        max_value = std::max( max_value, value );
    }

    return {min_value, max_value};
}

// fallback generic function definition:
// (input type === output type) => simple pass-through function.
template<typename cell_t>
template<typename as_t>
inline std::vector<as_t> HeightLayer<cell_t>::data() const {
    static_assert( std::is_same<cell_t, as_t>::value, "input type must be equal to output type");
    return std::vector<as_t>( heights_ );
}


// function specialization: bytes -> floats
template<>
template<>
inline std::vector<float> HeightLayer<uint8_t>::data<float>() const { 
    // this should be a copy constructor (#(5))
    // auto-casts the internal data
    return std::vector<float>(  heights_.cbegin(), heights_.cend() );
}

// function specialization: floats -> bytes
// downcasting -> requires scaling down values
template<>
template<>
inline std::vector<uint8_t> HeightLayer<float>::data<uint8_t>() const {
    // initialize-allocate destination vector:
    std::vector<uint8_t> bytes( heights_.size() );

    const auto [min, max] = get_min_max();
    const float range = max - min;

    // simple way to transform
    for( size_t i = 0; i < heights_.size(); ++i ){
        bytes[i] = static_cast<uint8_t>(((heights_[i] - min)/range) * 255.0);
    }

    // fancy (and broken -.-) way to transform:
    // // copy and transform floats into bytes:
    // std::transform( 
    //     const_cast<float*>(data_start),
    //     const_cast<float*>(data_end),
    //     std::back_inserter(bytes),
    //     [](float& from){ return static_cast<uint8_t>(from); }
    //     // [&min,&range](float& from){ return static_cast<uint8_t>(((from - min)/range) * 255.0); }
    // );

    return bytes;
}
    

// } // namespace Terrain
