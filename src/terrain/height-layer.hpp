//Copyright (c) 2022, MIT License, Daniel Williams
#pragma once

#include <algorithm>
#include <array>
#include <cstring>
#include <vector>
// #include <utility>

#include "coordinate.hpp"
#include "material.hpp"

namespace Terrain { 

using GridCoordinate=::Terrain::GridCoordinate;

template< typename _cell_type_t >
class HeightLayer {
public:
    // \brief All cells in this layer of this type. Use this type for accessing/consuming this layer
    using cell_t = _cell_type_t;

public:
    HeightLayer( uint32_t _size_i, uint32_t _size_j, float _cell_width_m, material_t _material )
        : cell_width_m_(_cell_width_m)
        , material_(_material)
        , sizes_(_size_i,_size_j)
    {
        heights_.resize( _size_i*_size_j );
        std::fill( heights_.begin(), heights_.end(), static_cast<cell_t>(0) );
    }

    ~HeightLayer() = default;

    float cell_width() const { return cell_width_m_; }

    /// \brief check if the given grid coordinate falls within the boundary of this terrain
    ///
    // \warning this test relies on `static_cast<uint8_t>(-1)` wrapping around to a very high number for unsigned integers
    // \warning -- this is platform dependent behavior! (2s-complement representation) --tested on x86_64 + gcc12 + c++23
    float contains( const GridCoordinate& c ) const {
        return ( (c.first < size_i()) && (c.second < size_j()) );
    }

    // this function may copy and/or transform its data...
    // -- so it needs to handle ownership -> std::vector
    template<typename as_t>
    inline std::vector<as_t> data() const;

    /// \brief get minimum and maxium heights
    /// 
    /// \return std::pair( min, max )
    std::pair<cell_t,cell_t> get_min_max() const;


    cell_t& height( float x, float y ) {
        const GridCoordinate c( x, y, cell_width_m_);
        return heights_[ index( c ) ];
    }

    cell_t height( float x, float y ) const {
        const GridCoordinate c( x, y, cell_width_m_);
        return heights_[ index( c ) ];
    }

    cell_t& height( uint32_t i, uint32_t j ) {
        return heights_[ index( GridCoordinate( i, j ) ) ];
    }

    cell_t height( uint32_t i, uint32_t j ) const {
        return heights_[ index( GridCoordinate( i, j ) ) ];
    }

    inline cell_t& height( uint32_t i ) {
            return heights_[i];
    }

    inline cell_t height( uint32_t i ) const {
            return heights_[i];
    }

    cell_t& height( const GridCoordinate& c ) {
        return heights_[ index( c ) ];
    }

    cell_t height( const GridCoordinate& c ) const {
        return heights_[ index( c ) ];
    }

    inline uint32_t index( const GridCoordinate& c ) const { return c.first + c.second * sizes_.first; }

    const std::string& get_layer_type() const { return "heights"; }

    material_t material() const { return material_; }

    float spacing() const { return cell_width_m_; }

    uint32_t size() const { return heights_.size(); }
    uint32_t size_i() const { return sizes_.first; }
    uint32_t size_j() const { return sizes_.second; }

    float width_x() const { return cell_width_m_*sizes_.first; }
    float width_y() const { return cell_width_m_*sizes_.second; }

// ====== ====== External Functions ====== ====== 
public:
    std::string name;

private:
    const float cell_width_m_;

    const material_t material_;

    const std::pair<uint32_t,uint32_t> sizes_;

private:
    std::vector<cell_t> heights_;

};


#include "height-layer.inl"

}   // namespace Terrain
