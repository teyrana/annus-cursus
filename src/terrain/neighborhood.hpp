//Copyright (c) 2022, MIT License, Daniel Williams

#pragma once

#include <array>
#include <cstdint>

namespace Terrain {


// \brief describes the neighbors of a current cell, in terms of the relative indices
struct Neighbor {
    int8_t column_offset;
    int8_t row_offset;
    // normalized for closest neighbor === 100 === 1.0
    // treat this as an 8-bit fixed-point number.
    uint8_t distance;
};

static constexpr std::array< struct Neighbor, 8> moore_neighborhood = {
                Neighbor{  1,  0, 100 },
                Neighbor{  1,  1, 144 },
                Neighbor{  0,  1, 100 },
                Neighbor{ -1,  1, 144 },
                Neighbor{ -1,  0, 100 },
                Neighbor{ -1, -1, 144 },
                Neighbor{  0, -1, 100 },
                Neighbor{  1, -1, 144 }
};

static constexpr std::array< struct Neighbor, 20> radius_2_neighborhood = {
                // adjacent / radius=1
                Neighbor{  1,  0, 100 },
                Neighbor{  1,  1, 144 },
                Neighbor{  0,  1, 100 },
                Neighbor{ -1,  1, 144 },
                Neighbor{ -1,  0, 100 },
                Neighbor{ -1, -1, 144 },
                Neighbor{  0, -1, 100 },
                Neighbor{  1, -1, 144 },

                // Outer East Ring
                Neighbor{  2, -1, 223 },
                Neighbor{  2,  0, 200 }, 
                Neighbor{  2,  1, 223 },
                // Outer North Ring
                Neighbor{  1,  2, 223 },
                Neighbor{  0,  2, 200 },
                Neighbor{ -1,  2, 223 },
                // Outer West Ring
                Neighbor{ -2,  1, 223 },
                Neighbor{ -2,  0, 200 },
                Neighbor{ -2, -1, 223 },
                // Outer South Ring
                Neighbor{ -1, -2, 223 },
                Neighbor{  0, -2, 200 },
                Neighbor{  1, -2, 223 }
};

static constexpr std::array< struct Neighbor, 4> von_neumann_neighborhood = {
                Neighbor{  1,  0, 100 },
                Neighbor{  0,  1, 100 },
                Neighbor{ -1,  0, 100 },
                Neighbor{  0, -1, 100 }
};

};
