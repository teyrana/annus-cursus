//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <string>
#include <sstream>

#include "unit-layer.hpp"

using Terrain::UnitLayer;

const std::string UnitLayer::layer_type_names[5] = {
    "AIR_UNIT",
    "BUILDING_UNIT",
    "GROUND_UNIT",
    "SUBMARINE_UNIT",
    "SUBTERRAIN_UNIT"
    "UNKNOWN_UNIT", 
};

UnitLayer::UnitLayer( UNIT_TYPE unit_layer_type, std::pair<uint32_t,uint32_t> _sizes )
        : layer_type_(unit_layer_type)
        , sizes_(_sizes)
{
    occupied_.resize( sizes_.first * sizes_.second );
    std::fill( occupied_.begin(), occupied_.end(), static_cast<cell_t>(0) );
}

std::string UnitLayer::name() const {
    return "UnitLayer::" + get_layer_type();
}

const std::string& UnitLayer::get_layer_type() const {
    return layer_type_names[ layer_type_ ];
}

std::string UnitLayer::str() const {
    std::ostringstream ss;
    const std::string indent = "    ";
    ss << indent << "====== ====== "
        << "UnitLayer::" << get_layer_type()
        << " (" << sizes_.first << " x " << sizes_.second << ")"
        << " ====== ======\n";

    for( uint32_t i = 0; i < sizes_.first; ++i ){
        ss << indent << indent;
        for( uint32_t j = 0; j < sizes_.second; ++j ){
            const cell_t value = occupied_[i*sizes_.second + j];
            if( 0 == value ){
                ss << ". ";
            }else if( isalnum(value) ){
                ss << static_cast<char>(value) << " ";
            }else{
                ss << value << " ";
            }
        }
        ss << "\n";
    }

    ss << indent << "====== ====== ====== ====== ====== ======\n";
    return ss.str();
}