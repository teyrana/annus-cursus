//Copyright (c) 2023, MIT License, Daniel Williams

#include <cstdint>
#include <cstdio>
#include <iostream>
#include <filesystem>
#include <fstream>

#include <cxxopts.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include "catalog/index.hpp"
#include "engine/entity.hpp"
#include "engine/state.hpp"
#include "terrain/map.hpp"
#include "terrain/coordinate.hpp"

using Engine::Entity;
using Terrain::Map;
using Coordinate = Terrain::GridCoordinate;

// ====== Global Variables ======
int verbosity = 0;

// ============ ============ Function Definitions  ============ ============


// bool load_map( const std::string& map_name, std::unique_ptr<Terrain::Map>& /*map*/ ){
//     if( map_name.empty() ){
//         std::cerr << "<< Invalid map name: " << map_name << std::endl;
//         return false;
//     }

//     const auto map_path = std::filesystem::path("data/maps") / map_name;
//     if( not std::filesystem::exists( map_path ) ){
//         std::cerr << "<< Map does not exist: " << map_path << std::endl;
//         std::cerr << "<< Aborting!" << std::endl;
//         return false;
//     }

//     // const std::string map_name = doc.value("map", "default-map-name");

//     // std::cerr << ">> Loading map: " << map_name << std::endl;
//     // if( doc.contains("height") and doc["height"].is_number_unsigned() ){
//     //     map_height = doc["height"].get<uint32_t>();
//     // }
//     // if( doc.contains("width") and doc["width"].is_number_unsigned() ){
//     //     map_width = doc["width"].get<uint32_t>();
//     // }
//     // if( (0<map_width) && (0<map_height) ){
//     //     map = std::make_unique<Terrain::Map>( name, map_width, map_height );
//     // }else{
//     //     std::cerr << "<< Invalid map sizes: " << map_width << "x" << map_height << std::endl;
//     //     return false;
//     // }

//     // ... ( NYI )

//     return false;
// }

bool load_building( const nlohmann::json& obj, Engine::State& state ){
    if( not ( obj.contains("from") && obj.contains("at") )){
        std::cerr << "!! Invalid building object: " << obj << std::endl;
        return false;
    }
    const auto& at_obj = obj["at"];
    if( (not at_obj.is_array()) || (2 != at_obj.size()) ){
        std::cerr << "!! 'at' key is incorrect size!  Expected a list of two elements!" << std::endl;
    }

    const uint32_t pattern_index = obj["from"].get<uint32_t>();

    const Coordinate at_coord = { at_obj[0], at_obj[1] };

    const uint32_t eid = state.emplace_building( pattern_index, at_coord );
    if( Entity::ERROR_ENTITY_ID == eid ){
        std::cerr << "!! Failed to emplace building @ EID=" << eid << std::endl;
        return false;
    }

    // extend poperties here

    return true;
}


bool load_unit( const nlohmann::json& obj, Engine::State& state ){

    if( not ( obj.contains("from") && obj.contains("at") )){
        std::cerr << "!! Invalid building object: " << obj << std::endl;
        return false;
    }
    const auto& at_obj = obj["at"];
    if( (not at_obj.is_array()) || (2 != at_obj.size()) ){
        std::cerr << "!! 'at' key is incorrect size!  Expected a list of two elements!" << std::endl;
    }

    const uint32_t pattern_index = obj["from"].get<uint32_t>();

    const std::pair<uint32_t, uint32_t> at_coord = { at_obj[0], at_obj[1] };

    const uint32_t eid = state.emplace_unit( pattern_index, at_coord );
    if( Entity::ERROR_ENTITY_ID == eid ){
        std::cerr << "!! Failed to emplace unit @ EID=" << eid << std::endl;
        return false;
    }

    // extend poperties here

    return true;
}


/// \param assume "obj" is the "state" object from the save file. 
/// \param state The state object to load into.
bool load_state( const nlohmann::json& obj, Engine::State& state ){
    std::cout << "    >> Loading state..." << std::endl;

    { 
        const std::string key = {"building"};
        if( obj.contains(key) && obj[key].is_array() ){
            std::cout << "        >> Loading Each Building..." << std::endl;
            for( const auto& bldg : obj[key] ){
                if( not load_building( bldg, state ) )
                    return false;            
            }
        }
    }{
        const std::string key = {"unit"};
        if( obj.contains(key) && obj[key].is_array() ){
            std::cout << "        >> Loading Each Unit..." << std::endl;
            for( const auto& bldg : obj[key] ){
                if( not load_unit( bldg, state ) )
                    return false;
            }
        }
    }

    return true;
}

bool load_save_file( const std::filesystem::path& load_path, Engine::State& state ){

    std::ifstream load_file( load_path );
    if( not load_file.is_open() ){
        fprintf( stderr, "XXX Failed to open file: %s\n", load_path.c_str() );
        return false;
    }

    try {
        auto doc = nlohmann::json::parse(load_file, nullptr, false, true);
        if (doc.is_discarded()) {
            return false;
        }

//         if( not doc.contains("map") ){
//             std::cerr << "!! Fatal -- No map specified in save file!!" << std::endl;
//             return false;
//         }else if( doc["map"].is_string() ){
//             const std::string map_name = doc.value("map", "default-map-name");
//             if( not load_map( map_name, map ) ){
//                 std::cerr << "<< Failed to load map: " << map_name << std::endl;
//                 return false;
//             }
//         }

        if( ! doc.contains("state") ){
            std::cerr << "!! Fatal -- Save file does not contain any state!!" << std::endl;
            return false;
        }else if( doc["state"].is_object() ){
            if( not load_state( doc["state"], state ) ){
                std::cerr << "<< Failed to load units" << std::endl;
                return false;
            }
        }

        return true;
    } catch (const std::exception& ex) {
        std::cerr << "error while parsing save file!?: " << ex.what() << std::endl;
        return false;
    }
}



// ============ ============   Main   ============ ============
int main(int argc, char *argv[]) {
    cxxopts::Options options("TileGen", "Procedural Map Generator");
    options.add_options()
        ("l,load", "load this saved game file", cxxopts::value<std::string>()->default_value(""))
        ("m,map", "Output map path", cxxopts::value<std::string>()->default_value("")->implicit_value("blank"))
        ("help", "Print usage")
        ("v,verbose", "Increase verbosity of output", cxxopts::value<int>())
        ;
    const auto parsed = options.parse(argc, argv);

    // # Processing Command Line Args
    // std::cout << "# ==== Processing Command Line Args ====" << std::endl;

    if (parsed.count("help")){
      std::cout << options.help() << std::endl;
      exit(0);
    }

    verbosity = parsed.count("verbose");

    const std::filesystem::path load_game_path( parsed["load"].as<std::string>() );
    const std::filesystem::path load_map_path( parsed["map"].as<std::string>() );

    // const std::filesystem::path map_output_path( parsed["map"].as<std::string>() );
    // const std::filesystem::path preview_color_path( parsed["color"].as<std::string>() );

    // # First, load the game engine data:
    // ========================================
    Catalog::Index catalog;
    std::cout << ">>> Loading Catalog:" << std::endl;
    if( not catalog.load("data/catalog/")){
        std::cerr << "XXX:Failed to load game data!!" << std::endl;
        return 1;
    }

    std::unique_ptr<Terrain::Map> map = std::make_unique<Terrain::Map>();
    std::unique_ptr<Engine::State> state = std::make_unique<Engine::State>( catalog, *map );

    std::cout << ">>> Starting with a blank map, and empty state." << std::endl;
    // Option 1: NYI: Load a save game and map
    if( std::filesystem::exists( load_game_path ) ){
        std::cout << ">>> Loading Game: " << load_game_path << std::endl;
        if( not load_save_file( load_game_path, *state ) ){
            std::cerr << "XXX:Failed to load game:!!" << std::endl;
            return 11;
        }
    }

    // // Option 2: NYI: Load a map and create a new game
    // }else if( std::filesystem::exists( load_map_path ) ){
    //     std::cout << ">>> Loading (just) Map: " << load_map_path << std::endl;
    //     // // implement a function, here, to load just the map:
    //     // map = std::make_unique<Terrain::Map>( load_map_path );
    //     // state = std::make_unique<GameState>( *map );


    { // DEBUG
        // DEBUG: Print out the initial map Size:
        std::cout << "    ::Dimensions: " << map->size_i() << " x " << map->size_j() << std::endl;
        // ... and initial terrain? 

        // placeholder -- should eventually implement a ncurses map renderer
        std::cout << map->str() << std::endl;
    }


    // DEBUG // placeholder
    state->update();

    // // run game 
    // (NYI)

    return 0;
}
