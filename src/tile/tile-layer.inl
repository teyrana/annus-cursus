

// implicit includes, from header
// implicit namespace: Tile

template<typename height_layer_t>
void TileLayer::fill( height_layer_t source_layer, const TileMap& map ){
    // now, let's actually apply the mapping from the source layer to the tile layer
    // make sure to reverse the rows, filling from top-to-bottom, so that the tilemap renders correctly
    for( uint32_t j = 0; j < source_layer.size_j(); ++j ){
        const uint32_t read_row = j;
        const uint32_t write_row = (source_layer.size_j()-1) - j;
        for( uint32_t i = 0; i < source_layer.size_i(); ++i ){
            const uint32_t read_col = i;
            const float from_height = source_layer.height( read_col, read_row );
            
            // calculate the write value
            const tile_t tid = map.get_tile( from_height );

            // write to this layer
            tile( read_col, write_row ) = tid;
        }
    }
}