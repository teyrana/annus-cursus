//Copyright (c) 2023, MIT License, Daniel Williams


#include <cstdint>

namespace Tile {

using patch_index_t = uint32_t;


class TilePatch {
public:
    TilePatch( uint8_t ax, uint8_t ay, uint8_t sx, uint8_t sy )
        : at(ax,ay)
        , size(sx,sy)
    {}

public:
    // location of tile-patch within the tile sheet; in tile units
    const std::pair<uint8_t,uint8_t> at;

    /// \brief dimensions of this patch, in tile units
    const std::pair<uint8_t,uint8_t> size;

};



}   // namespace Tile