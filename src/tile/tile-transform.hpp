//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include "tile-layer.hpp"
#include "tile-type.hpp"

namespace Tile {

class TilePatchTransform {

public:
    using Neighbor = std::pair<int,int>;
    static constexpr std::array< Neighbor, 8> moore_neighborhood = {
        Neighbor{ 0, 1}, // N
        Neighbor{ 1, 1}, // NE
        Neighbor{ 1, 0}, // E
        Neighbor{ 1,-1}, // SE
        Neighbor{ 0,-1}, // S
        Neighbor{-1,-1}, // SW
        Neighbor{-1, 0}, // W
        Neighbor{-1, 1}, // NW
    };

    TilePatchTransform( tile_t tid )
        : center_(tid)
    {}

    tile_t center() const { return center_; }

    tile_t key() const { return center_; }

    bool matches( const TileLayer& layer, uint32_t i, uint32_t j);


private:
    tile_t center_;

    tile_t neighbors[];


};



}   // namespace Tile
