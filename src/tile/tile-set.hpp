//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "patch-link.hpp"
#include "tile-type.hpp"
#include "tile-patch.hpp"

namespace Tile {

/// \brief represents a collection of tiles that can be generate further features
class TileSet {
public:
    TileSet() = delete;

    TileSet( const std::filesystem::path& _tileset_path );

    ~TileSet() = default;

    bool exists() const;

    bool load();

    std::string name() const { return name_; }

    std::string tiled_definition_path() const { return tiled_definition_path_; }

    uint32_t offset() const { return tile_index_offset_ ; }
    void offset( uint32_t _offs ) { tile_index_offset_ = _offs; }

    uint32_t tile_height() const { return tile_size_.first; }
    uint32_t tile_width() const { return tile_size_.second; }

    uint32_t size() const { return 0; }
private:

    bool load_link_entry( const nlohmann::json& link_entry );
    bool load_links( const nlohmann::json& link_list );

    bool load_metadata_config();

    bool load_patch_entry( const nlohmann::json& patch_entry );
    bool load_patches( const nlohmann::json& patch_list );

    bool load_tiled_definition();

private:
    const std::string name_;

    /// \brief refers to the png image containing the tiles themselves
    const std::string tile_sheet_path_;

    /// \brief contains the tileset definition for Tiled.  Mostly for passing to output
    const std::string tiled_definition_path_;

    /// \brief contains tile metada, including tile patches and patch linking.
    const std::string config_path_;

    // WARNING -- this is ua hack.... we should have a proper definition of this.
    // or some way to pin it down ?  :/
    static constexpr char data_relpath[] = "data/";

    static constexpr char tilesheet_suffix[] = ".png";
    static constexpr char tiled_suffix[] = ".tsj";
    static constexpr char config_suffix[] = ".json";

    uint32_t tile_count_;
    uint32_t tile_index_offset_;
    std::pair<uint32_t, uint32_t> tile_size_;

    std::vector<TilePatch> patches_;
    std::vector<PatchLink> links_;

};


}   // namespace Tile
