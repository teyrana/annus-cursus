//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <memory>
#include <vector>

#include "tile-layer.hpp"
#include "tile-type.hpp"
#include "tile-set.hpp"


namespace Tile {

class Tiling {
public:
    Tiling() = delete;

    Tiling( uint32_t width, uint32_t height)
        : size( width, height)
        , tile( 1, 1 )
    {}

    ~Tiling() = default;

    TileLayer& emplace_layer(){ 
        layers.emplace_back( size.first, size.second, tile.first );
        return layers.back();
    }

public:
    std::vector<Tile::TileLayer> layers;

    std::vector<Tile::TileSet> tilesets;

    // number of tiles across and up the map
    std::pair<uint32_t, uint32_t> size;

    // size of each tile, in pixels
    std::pair<uint32_t, uint32_t> tile;

};


}   // namespace Tile
