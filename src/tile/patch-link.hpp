//Copyright (c) 2023, MIT License, Daniel Williams

#include <cstdint>

namespace Tile {

struct PatchLink {
public:
    uint32_t link_from_patch;
    uint32_t link_to_patch; 
    uint8_t offset_x;
    uint8_t offset_y;
};

}   // namespace Tile