//Copyright (c) 2023, MIT License, Daniel Williams

#include <cmath>

#include <cstdio>

#include "tile-map.hpp"

using Tile::tile_t;
using Tile::TileMap;


TileMap::TileMap()
    : default_tile_id_( Tile::default_tile_id )
{}


// adds this mapping-based range to the height map in this class
void TileMap::add_range( float min, float max, tile_t tile_id ){

    // update default value -- but do not add a matcher
    if( std::isnan(min) && std::isnan(max) ){
        default_tile_id_ = tile_id;
        return;
    }
    
    std::function<tile_t(float)> matcher = nullptr;
    if( std::isnan(min) ){
        // less-than range
        matcher = [=](float value){ return (value <= max)?tile_id:error_tile_id; };
    } else if( std::isnan(max) ){
        // greater-than range
        matcher = [=](float value){ return (min <= value)?tile_id:error_tile_id; };
    } else {
        // between range
        matcher = [=](float value){ return (max <= value) && (value <= min)?tile_id:error_tile_id; };
    }
    height_matchers_.emplace_back( matcher );
}


tile_t TileMap::get_tile( float value ) const { 
    for( auto& matcher : height_matchers_ ){
        const tile_t tid = matcher(value);
        if( Tile::error_tile_id != tid ){
            return tid;
        }
    }

    return default_tile_id_;
}