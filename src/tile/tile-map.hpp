//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <cstdint>
#include <functional>
#include <map>

#include "tile-type.hpp"

namespace Tile {

class TileMap {
public:
    static constexpr auto less_than_matcher = [](float value, float max ){
        return (value <= max);
    };

    static constexpr auto greater_than_matcher = [](float value, float min ){
        return (min <= value);
    };

    static constexpr auto between_matcher = [](float value, float min, float max){
        return ((min <= value) && (value <= max));
    };


public:
    TileMap();

    ~TileMap() = default;

    void add_range( float min, float max, tile_t tile_id );

    tile_t get_tile( float value ) const;

    size_t size() const { return height_matchers_.size(); }

private:
    // a list of functions mapping height ranges to tile ids
    std::vector<std::function<tile_t(float)>> height_matchers_;

    // value to return if no match is found
    tile_t default_tile_id_;

};


}   // namespace Tile
