//Copyright (c) 2023, MIT License, Daniel Williams

#include <cstdio>
#include <filesystem>
#include <fstream>
#include <string>

#include "tile-set.hpp"
#include <nlohmann/json.hpp>


using Tile::TileSet;

TileSet::TileSet( const std::filesystem::path& _tileset_path )
    : name_(_tileset_path.stem())
    , tile_sheet_path_( _tileset_path.parent_path() / (name_ + tilesheet_suffix))
    , tiled_definition_path_( _tileset_path.parent_path() / (name_ + tiled_suffix))
    , config_path_( _tileset_path.parent_path() / (name_ + config_suffix))
    , tile_count_(0)
    , tile_index_offset_(0)
    , tile_size_{0,0}
{}

bool TileSet::exists() const {

    if ( !std::filesystem::exists( tile_sheet_path_ ) ) {
        fprintf( stderr, "!!! Missing an expected Tileset file: %s !!<<\n", tile_sheet_path_.c_str() );
        return false;
    }

    if ( !std::filesystem::exists( tiled_definition_path_ ) ) {
        fprintf( stderr, "!!! Missing an expected Tileset file: %s !!<<\n", tiled_definition_path_.c_str() );
        return false;
    }

    if( ! std::filesystem::exists( config_path_ ) ) {
        fprintf( stderr, "!!! Missing an expected Tileset file: %s !!<<\n", config_path_.c_str() );
        return false;
    }

    return true;
}


bool TileSet::load(){
    return ( load_tiled_definition() && load_metadata_config());
}


bool TileSet::load_link_entry( const nlohmann::json& entry ){
    if( ! entry.is_object())
        return false;

    if( (!entry.contains("from")) || (!entry.contains("to")) || (!entry.contains("offs")) ){
        return false;
    }

    const int from_index = entry["from"].get<int>();

    const int to_index = entry["to"].get<int>();

    const auto& offsets = entry["offs"];

    if( (!offsets.is_array()) || (2 != offsets.size())){
        return false;
    }

    const int offx = offsets[0].get<int>();
    const int offy = offsets[1].get<int>();

    links_.emplace_back( from_index, to_index, offx, offy );

    return true;
}

bool TileSet::load_links( const nlohmann::json& links ){
    for( const auto& entry : links ){
        if( load_link_entry(entry) ){
            continue;
        }else{
            return false;
        }
    }

    return true;
}

bool TileSet::load_metadata_config() {
    try{
        std::ifstream inf( config_path_ );

        auto doc = nlohmann::json::parse( inf );

        if( ! doc.is_object() ){
            printf( "!? tileset config file could not be parsed!?: %s \n", config_path_.c_str() );
            return false;
        }

        if( doc.contains("patches") && doc["patches"].is_array() ){
            if( ! load_patches( doc["patches"] )){
                fprintf( stderr, "<!! Could not load links!! Aborting.\n");
                return false;
            }
        }

        if( doc.contains("links") && doc["links"].is_array() ){
            if( ! load_links( doc["links"] )){
                fprintf( stderr, "<!! Could not load links!! Aborting.\n");
                return false;
            }
        }

        return true;

    // }catch( FileNotFound
        // not sure what the correct name is....
    }catch ( nlohmann::json::parse_error& ex ){
        fprintf( stderr,  "!? Could not parse JSON file: %s\n", ex.what() );
    }
    return false;
}


bool TileSet::load_patch_entry( const nlohmann::json& entry ){
    if( ! entry.is_object())
        return false;
    

    if( (!entry.contains("at")) || (!entry.contains("sz") )){
        return false;
    }
    
    const auto& ats = entry["at"];
    const auto& sizes = entry["sz"];

    if( (!ats.is_array()) || (2 != ats.size()) ){
        return false;
    }

    if( (!sizes.is_array()) || (2 != sizes.size()) ){
        return false;
    }

    const int atx = ats[0].get<int>();
    const int aty = ats[1].get<int>();
    const int szx = sizes[0].get<int>();
    const int szy = sizes[1].get<int>();

    patches_.emplace_back( atx, aty, szx, szy );

    return true;
}

bool TileSet::load_patches( const nlohmann::json& patches ){
    for( const auto& entry : patches ){
        if( load_patch_entry(entry) ){
            continue;
        }else{
            return false;
        }
    }

    return true;
}


bool TileSet::load_tiled_definition() {
    try{
        std::ifstream inf( tiled_definition_path_ );

        auto doc = nlohmann::json::parse( inf );

        if( ! doc.is_object() ){
            printf( "!? tileset config file could not be parsed!?: %s \n", tiled_definition_path_.c_str() );
            return false;
        }

        tile_size_ = { doc.value("tileheight", 1),
                        doc.value("tilewidth", 1)};

        const uint32_t image_width_px = doc.value("imagewidth", 1);
        const uint32_t tiles_across = image_width_px / tile_size_.first;
        const uint32_t image_height_px = doc.value("imageheight", 1);
        const uint32_t tiles_up = image_height_px / tile_size_.second;

        tile_count_ = tiles_across * tiles_up;

        return true;

    // }catch( FileNotFound
        // not sure what the correct name is....
    }catch ( nlohmann::json::parse_error& ex ){
        fprintf( stderr,  "!? Could not parse JSON file: %s\n", ex.what() );
    }
    return false;
}

