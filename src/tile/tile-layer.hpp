//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <algorithm>
#include <array>
#include <cstring>
#include <vector>
#include <string>


#include "../terrain/coordinate.hpp"

#include "tile-map.hpp"
#include "tile-type.hpp"

namespace Tile {

using GridCoordinate=::Terrain::GridCoordinate;


class TileLayer {
public:
    TileLayer( uint32_t _size_i, uint32_t _size_j, float _cell_width_m )
        : cell_width_m_(_cell_width_m)
        , name_("TileLayer")
        , sizes_(_size_i,_size_j)
    {
        tiles_.resize( _size_i*_size_j );
        std::fill( tiles_.begin(), tiles_.end(), static_cast<tile_t>(1) );
    }

    ~TileLayer() = default;

    float cell_width() const { return cell_width_m_; }

    // \warning this test relies on `static_cast<uint8_t>(-1)` wrapping around to a very high number for unsigned integers
    // \warning -- this is platform dependent behavior! (2s-complement representation) --tested on x86_64 + gcc12 + c++23
    float contains( const GridCoordinate& c ) const {
        return ( (c.first < size_i()) && (c.second < size_j()) );
    }

    const std::vector<tile_t> data() const { return tiles_; }

    template<typename height_layer_t>
    inline void fill( height_layer_t layer, const TileMap& map );

    const std::string& name() const {  return name_; }

    tile_t& tile( uint32_t i, uint32_t j ) {
        return tiles_[ index( GridCoordinate( i, j ) ) ];
    }

    tile_t tile( uint32_t i, uint32_t j ) const {
        return tiles_[ index( GridCoordinate( i, j ) ) ];
    }

    tile_t& tile( const GridCoordinate& c ) {
        return tiles_[ index( c ) ];
    }

    tile_t tile( const GridCoordinate& c ) const {
        return tiles_[ index( c ) ];
    }

    inline uint32_t index( const GridCoordinate& c ) const { return c.first + c.second * sizes_.first; }

    float spacing() const { return cell_width_m_; }

    uint32_t size() const { return tiles_.size(); }
    uint32_t size_i() const { return sizes_.first; }
    uint32_t size_j() const { return sizes_.second; }

    float width_x() const { return cell_width_m_*sizes_.first; }
    float width_y() const { return cell_width_m_*sizes_.second; }

// ====== ====== External Functions ====== ====== 
private:
    const float cell_width_m_;

    std::string name_;

    const std::pair<uint32_t, uint32_t> sizes_;

private:
    std::vector<tile_t> tiles_;

};

#include "tile-layer.inl"

}   // namespace Tile
