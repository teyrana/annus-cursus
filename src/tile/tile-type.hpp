//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <cstdint>

namespace Tile {

// \brief All tiles are identified by this type.
using tile_t = uint32_t;

/// \brief any function returning this value indicates an error
/// 0 is never a valid tile id.
constexpr tile_t error_tile_id = 0;

/// \brief The default tile id
// (arbitrary number.  *shrug*)
constexpr tile_t default_tile_id = 1;


constexpr auto width = 16;
constexpr auto height = 16;

}   // namespace Tile
