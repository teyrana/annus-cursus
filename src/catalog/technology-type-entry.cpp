//Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <iostream>
#include <string>

#include "matchers.hpp"
#include "technology-type-entry.hpp"

using Catalog::TechnologyTypeEntry;

void TechnologyTypeEntry::update( const nlohmann::json& blob ){
    if( not blob.is_object() ){
        return;
    }

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            // pass -- ignore

        }else if( 0 == key.find("req") ){
            if( value.is_string() ){
                requires_.emplace(value.get<std::string>());
            }else if( value.is_array() ){
                for( const auto& ea : value ){
                    requires_.emplace(ea.get<std::string>());
                }
            }else{
                std::cerr << "!!!! Could not load 'requires' field for technology: " << name << std::endl;
            }

        }else if("tag" == key){
            tags.update(value);
        }else{
            std::cerr << "!!!! Could not load JSON key: " << key << " into class " << entry_type_name() << std::endl;
            return;
        }
    }
}


void TechnologyTypeEntry::update( const TechnologyTypeEntry& parent){
    tags.update(parent.tags);

    for( const auto& ea : parent.requires_ ){
        this->requires_.emplace(ea);
    }
}

std::string TechnologyTypeEntry::str() const {
    std::ostringstream buf;

    constexpr char indent[] = "\n        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str();
    }

    if( not description.empty() ){
        buf << indent << "    - " << description;
    }

    if( this->requires_.size()){
        buf << indent << "    :: Requires:";
        for( const auto& req : requires_ ){
            buf << indent << "        - " << req;
        }
    }

    return buf.str();
}

bool TechnologyTypeEntry::valid() const { 
    return true;
}

bool TechnologyTypeEntry::verify( const EntryTable<TechnologyTypeEntry>& techs ) const {
    bool valid = true;

    for( const auto& req : requires_ ){
        if( ! techs.contains(req) ){
            std::cerr << "    !! could not find 'prerequisite' tech: " << req << " from tech: " << key << std::endl;
            valid = false;
        }
    }

    return valid;
}