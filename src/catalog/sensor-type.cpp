// Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <string>

#include "matchers.hpp"
#include "sensor-type.hpp"

using Catalog::SensorType;
using Catalog::match_any;


std::string SensorType::describe() const {
    switch(value){
        default:
        case SensorType::UNKNOWN:
            return "Unknown";
        case SensorType::VISUAL:
            return "Visual-spectrum; Human Vision; default";
        case SensorType::INFRARED:
            return "Infrared, Including Night Vision";
        case SensorType::AIRBORNE:
            return "Airborne; can see over walls cliffs, and other terrain";
        case SensorType::RADAR:
            return "Radar: Can see through Fog, Rain, Dust, etc";
        case SensorType::ELECTRONIC:
            return "Electronic: detects any sort of electronic emisions, like radio, cell phones, RADAR, etc";
    }
}

std::string SensorType::name() const {
    switch(value){
        case SensorType::UNKNOWN: return "Unknown";
        case SensorType::VISUAL: return "Visual";
        case SensorType::INFRARED: return "Infrared";
        case SensorType::AIRBORNE: return "Airborne";
        case SensorType::RADAR: return "Radar";
        case SensorType::ELECTRONIC: return "Electronic";
        default: return "Unknown";
    }
}

SensorType SensorType::parse( const std::string& text ){
    if( text.empty() ){
        return SensorType::UNKNOWN;
    }else if( 1 == text.size() ){
        switch(tolower(text[0])){
            default:
            case SensorType::UNKNOWN:
                return SensorType::UNKNOWN;
            case SensorType::INFRARED:
                return SensorType::INFRARED;
            case SensorType::AIRBORNE:
                return SensorType::AIRBORNE;
            case SensorType::RADAR:
                return SensorType::RADAR;
            case SensorType::ELECTRONIC:
                return SensorType::ELECTRONIC;
        }
    }

    // convert parameter string to lower case
    std::string lowkey = text;
    std::transform(lowkey.begin(), lowkey.end(), lowkey.begin(), ::tolower);


    if( match_any( lowkey, {"eye", "visual"} )){
        return SensorType::VISUAL;
    }else if(lowkey == "ir"){
        return SensorType::INFRARED;
    }else if(lowkey == "air"){
        return SensorType::AIRBORNE;
    }else if(lowkey == "radar"){
        return SensorType::RADAR;
    }else if(lowkey == "ew"){
        return SensorType::ELECTRONIC;
    }
    return SensorType::UNKNOWN;
}
