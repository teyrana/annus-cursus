//Copyright (c) 2023, MIT License, Daniel Williams

#include <iomanip>
#include <iostream>
#include <sstream>

#include "matchers.hpp"
#include "parsers.hpp"
#include "platform-type-entry.hpp"
#include "units.hpp"

using Catalog::PlatformTypeEntry;
using Catalog::Units;

std::string PlatformTypeEntry::str() const {
    std::ostringstream buf;

    constexpr char indent[] = "        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description  << '\n';
    }

    if( ! super.empty() ){
        buf << indent << "    - parent: " << super  << '\n';
    }

    buf << indent << "    - armor:     " << std::fixed << std::setprecision(2) << armor_ << '\n';
    buf << indent << "    - box:       " << bounds_.str() << '\n';
    buf << indent << "    - hitpoints: " << std::fixed << hitpoints_ << '\n';
    buf << indent << "    - mass:      " << std::fixed << mass_ << ' ' << Units::MASS.name() << '\n';
    buf << indent << "    - mobility:  " << std::fixed << mobility_.name() << '\n';


    if( 0 < modules_available_.size() ){
        buf << indent << "        - modules:\n";
        for( const auto& mod : modules_available_ ){
            buf << indent << "            - " << mod << '\n';
        }
    }

    if( 0 < mounts_available_.size() ){
        buf << indent << "        - mounts:\n";
        for( const auto& [mnt,qty] : mounts_available_ ){
            buf << indent << "            - " << mnt.name() << ": " << qty << "x\n";
        }
    }

    buf << indent << "    - volume:    " << std::fixed << volume_ << ' ' << Units::VOLUME.name() << '\n';

    return buf.str();
}

void PlatformTypeEntry::update( const nlohmann::json& blob ){
    if( ! blob.is_object() ){
        return;
    }

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            super = value.get<std::string>();

        }else if( "armor" == key ){
            armor_ = parse_as_float( value, armor_ );

        }else if( match_any( key, {"box", "size"})){
            if( value.is_array() && (3==value.size())){
                bounds_.height = parse_as_float( value[0], bounds_.width );
                bounds_.length = parse_as_float( value[1], bounds_.width );
                bounds_.width = parse_as_float( value[2], bounds_.width );
            }
        }else if( "height" == key ){
            bounds_.height = parse_as_float( value, bounds_.height );
        }else if( "length" == key ){
            bounds_.length = parse_as_float( value, bounds_.length );
        }else if( "width" == key ){
            bounds_.width = parse_as_float( value, bounds_.width );

        }else if ( "mass" == key) {
            mass_ = parse_as_float( value, mass_ );

        }else if( match_start( key, "mod")){
            if( value.is_array() ){
                for( const auto& mod : value ){
                    if( mod.is_string() ){
                        modules_available_.emplace( mod.get<std::string>() );
                    }
                }
            }
        }else if( "mount" == key ){
            if( value.is_object() ){
                for( const auto& [mnt,qty] : value.items() ){
                    const MountType mount = MountType::parse(mnt);
                    const int quantity = qty.get<int>();
                    mounts_available_.emplace( mount, quantity );
                }
            }
        }else if( match_any(key, {"move", "mobility"}) ){
            mobility_ = MobilityType::parse(value.get<std::string>());
        }else if( "volume" == key ){
            volume_ = parse_as_float( value, volume_ );

        }else{
              std::cerr << "!!!! Could not load JSON key: " << key << " into class: <" << entry_type_name() << ">: " << this->key << std::endl;
            return;
        }
    }
}

void PlatformTypeEntry::update( const PlatformTypeEntry& parent ){
    description = parent.description;
    tags.update(parent.tags);

    armor_ = parent.armor_;
    bounds_ = parent.bounds_;
    hitpoints_ = parent.hitpoints_;
    mass_ = parent.mass_;
    mobility_ = parent.mobility_;

    // perform a deep copy of the parent mounts set into this set:
    // mounts_available_ ( 

    modules_available_ = { parent.modules_available_ };
    volume_ = parent.volume_;
}



bool PlatformTypeEntry::valid() const {
    return true;
}
