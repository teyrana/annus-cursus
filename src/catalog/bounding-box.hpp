// Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class BoundingBox {
public:
    // x-dimension
    double length;
    // y-dimension
    double width;
    // z-dimension
    double height;

public:

    BoundingBox( float height, float width, float length )
        : length(length), width(width), height(height)
    {}

    BoundingBox( const BoundingBox& other )
        : length(other.length)
        , width(other.width)
        , height(other.height)
    {}

    void operator=( const BoundingBox& other ){
        length = other.length;
        width = other.width;
        height = other.height;
    }

    bool operator==( const BoundingBox& other ) const {
        return ( (length == other.length)
              && (width == other.width)
              && (height == other.height) );
    }

    std::string str() const {
        return std::string("[") + std::to_string(length) + ", " + std::to_string(width) + ", " + std::to_string(height) + "]";
    }
};

}   // namespace Catalog
