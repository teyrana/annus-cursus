//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <vector>


namespace Catalog {

inline bool match_start( const std::string& text, const std::string& prefix ){
    if( prefix.length() > text.length() ){
        return false;
    }

    return std::equal(prefix.begin(), prefix.end(), text.begin());
}

// test if the text matches any of the given strings
inline bool match_any( const std::string& text, const std::vector<std::string>& aliases ){
    for( const auto& alias : aliases ){
        if( text == alias ){
            return true;
        }
    }
    return false;
}


} // end namespace Catalog