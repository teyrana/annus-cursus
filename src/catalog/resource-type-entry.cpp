//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <iomanip>

#include "matchers.hpp"
#include "resource-type-entry.hpp"
#include "parsers.hpp"

using Catalog::ResourceTypeEntry;
using Catalog::StorageType;
using Catalog::Units;

// ==== ==== Class-Static Declarations ==== ====
const ResourceTypeEntry ResourceTypeEntry::RootResource; // using default constructor

// ==== ====   Function Definitions    ==== ====

std::string ResourceTypeEntry::str() const { 
    std::ostringstream buf;

    constexpr char indent[] = "        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description  << '\n';
    }
    
    if( ! super.empty() ){
        buf << indent << "    - parent: " << super  << '\n';
    }

    if ( Units::EACH_COUNT == units_.code() ){
        buf << indent << "    - units:      " << units_.name();
    }else if ( Units::KILOGRAMS_MASS == units_.code() ){
        buf << indent << "    - mass:   " << std::fixed << std::setw(4) << std::setprecision(2) << mass_ << ' ' << units_.name();
    }else if( Units::LITERS_VOLUME == units_.code() ){
        buf << indent << "    - volume: " << std::fixed << std::setw(4) << std::setprecision(2) << volume_ << ' ' << units_.name();
    }else if( Units::NONE == units_.code() ){
        // pass
    }else{
        buf << indent << "    - units:      " << units_.name();
    }

    buf << indent << "    - storage:    " << storage_.name();

    return buf.str();
}

void ResourceTypeEntry::update( const nlohmann::json& blob ){
    if( ! blob.is_object() ){
        return;
    }

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            super = value.get<std::string>();

        }else if( "units" == key ){
            units_ = Units::parse( value.get<std::string>());
        }else if("density" == key){ 
            if( blob.contains("units")){
                // if the units are available, make sure to load those _first_
                units_ = Units::parse( blob["units"].get<std::string>());
            }

            const float density = parse_as_float( value, NAN );
            if( std::isnan(density) ){
                std::cerr << "!!!! density is not a number. Aborting!!" << std::endl;
                return;
            }
            if( units_ == Units::KILOGRAMS_MASS ){
                mass_ = 1000.0;
                volume_ = 1.0 / density;
            }else if( units_ == Units::LITERS_VOLUME){
                mass_ = density;
                volume_ = 1.0;
            }
        }else if("mass" == key){
            mass_ = parse_as_float( value, mass_ );
        }else if( match_start( key, "vol")){
            volume_ = parse_as_float( value, volume_ );

        }else if( match_start( key, "stor") ){
            storage_ = StorageType::parse( value.get<std::string>() );

        }else{
            std::cerr << "!!!! Could not load JSON key: " << key << " into class: <" << entry_type_name() << ">: " << this->key << std::endl;
            return;
        }
    }

}

void ResourceTypeEntry::update( const ResourceTypeEntry& parent ){
    // override defaults with parent values
    mass_ = parent.mass_;
    storage_ = parent.storage_;
    tags.update(parent.tags);
    units_ = parent.units_;
    volume_ = parent.volume_;
}


bool ResourceTypeEntry::valid() const {
    return true;
}

