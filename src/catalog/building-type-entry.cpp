//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <sstream>

#include <nlohmann/json.hpp>

#include "matchers.hpp"
#include "building-type-entry.hpp"
#include "parsers.hpp"


using Catalog::BuildingTypeEntry;
using Catalog::FootPrint;
using Catalog::MountType;


std::string BuildingTypeEntry::str() const {
    std::ostringstream buf;
    static constexpr char indent[] = "        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]\n";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description << '\n';
    }
    
    if( ! super.empty() ){
        buf << indent << "    - parent: " << super << '\n';
    }

    if (0 < modules_available_.size()) {
        buf << indent << indent << "    - :mods: ";
        for (const auto& modl : modules_available_) {
            buf << modl << ", ";
        }
    }

    if (0 < mounts_available_.size()) {
        buf << indent << indent << "    - :mods: ";
        for (const auto& mnt : mounts_available_) {
            buf << mnt.name() << ", ";
        }
    }

    return buf.str();
}


void BuildingTypeEntry::update( const nlohmann::json& blob ){
    if( not blob.is_object() ){
        return;
    }

    mounts_available_.push_back( MountType::INTERNAL );

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            super = value.get<std::string>();
        }else if("armor" == key){
            armor_ = parse_as_float( value, armor_ );
        } else if ( (0 == key.find("foot")) || ("size"==key) ){
            if( value.is_array() && (2 == value.size()) ){
                footprint_.x = value[0].get<int>(); 
                footprint_.y = value[1].get<int>();
            }
        } else if (("hp" == key) || ("hitpoints" == key)) {
            hitpoints_ = value.get<int>();
        } else if ( 0 == key.find("module")) {
            if( value.is_string() ){
                modules_available_.insert(value.get<std::string>());
            } else if( value.is_array() ){
                modules_available_.insert(value.begin(), value.end());
            }
        }else if ( "mount" == key ){
            if( value.is_string() ){
                mounts_available_.push_back( MountType::parse(value.get<std::string>()) );
            }else if( value.is_array() ){
                for( auto & ea : value ){
                    mounts_available_.push_back( MountType::parse(ea.get<std::string>()) );
                }
            }
        } else if (key == "volume") {
            volume_ = parse_as_float( value, volume_ );
        }else{
            std::cerr << "!!!! Could not load key: " << key << " into: <" << entry_type_name() << ">: " << this->key << '\n';
            return;
        }
    }
}

void BuildingTypeEntry::update( const BuildingTypeEntry& parent ){
    armor_ = parent.armor_;
    footprint_ = parent.footprint_;
    hitpoints_ = parent.hitpoints_;
    mounts_available_ = parent.mounts_available_;
    modules_available_ = parent.modules_available_;
    tags.update(parent.tags);
}

bool BuildingTypeEntry::valid() const {
    return (0 < modules_available_.size());
}
