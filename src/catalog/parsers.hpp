//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

namespace Catalog {

inline float parse_as_bool( const nlohmann::json& parse_value, bool default_value ){

    if( parse_value.is_number() ){
        return static_cast<bool>(parse_value.get<int>());
    }else if( parse_value.is_string() ){
        const std::string text = parse_value.get<std::string>();
        if( text.empty() ){
            return default_value;
        }else if( 1 == text.size() ){
            switch(tolower(text[0])){
                case '1':
                case 't':
                case 'y':
                    return true;
                case '0':
                case 'f':
                case 'n':
                    return false;
                default:
                    return default_value;
            }
        }else if( text == "true" ){
            return true;
        }else if( text == "false" ){
            return false;
        }
    }

    return default_value;
}

inline float parse_as_float( const nlohmann::json& parse_value, float default_value ){
    if( parse_value.is_number() ){
        const float scratch = parse_value.get<float>();
        return (std::isnan(scratch)? default_value : scratch);
    }else if( parse_value.is_string() ){
        const std::string text = parse_value.get<std::string>();
        if( text.empty() ){
            return default_value;
        }

        const float scratch = std::stof( text );

        return (std::isnan(scratch)? default_value : scratch);
    }

    return default_value;
}


} // end namespace Catalog