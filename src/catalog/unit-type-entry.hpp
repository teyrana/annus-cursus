//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include "base-type-entry.hpp"
#include "entry-table.hpp"
#include "sensor-type.hpp"
#include "storage-type.hpp"
#include "tag-set.hpp"

namespace Catalog {

class UnitTypeEntry : public BaseTypeEntry<UnitTypeEntry> {
public:
    UnitTypeEntry()
        : UnitTypeEntry("Root Unit", 0, "root")
    {}

    UnitTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
        , mass_(100.0)
        // , platform_(...)
        // , modules_(...)
        , volume_(100.0)
    {}

    ~UnitTypeEntry() = default;

    static std::string entry_type_name(){ return "UnitTypeEntry";}

    float mass() const { return mass_; }

    std::string platform() const { return platform_; }

    std::map<std::string, int> modules() const { return modules_; }

    float volume() const { return volume_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const UnitTypeEntry& parent );

    template< typename module_table_t>
    bool verify( module_table_t modules ) const;

    bool valid() const;

private:
    // key to a platform-entry-type
    std::string platform_ = "<missing>";

    std::map<std::string, int> modules_;

    // all of these are synthetic values, calculated from the child platform & modules
    // std::vector<std::string> ammo;
    // SensorType sensor = SensorType::VISUAL;
    // Module module = Module::None;
    // Weapon weapon = Weapon::None;
    double mass_;
    double volume_;

}; // class UnitTypeEntry


template<typename module_table_t>
bool UnitTypeEntry::verify( module_table_t modtable ) const {
    // verify that all modules are available
    for( const auto& [modkey, _] : modules_ ) {
        if (!modtable.contains(modkey)) {
            std::cout << "    !! @<" << entry_type_name() << ">: " << key << "  ... could not find module: " << modkey << std::endl;
            return false;
        }
    }

    return true;
}

} // namespace Catalog
