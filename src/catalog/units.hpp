//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class Units {
public:
    enum UNITS_ENUM {
        EACH_COUNT =           'c',
        KILOGRAMS_MASS =       'm',
        LITERS_VOLUME =        'l',
        NEWTON_METERS_TORQUE = 't',
        NONE =                 'n',
        PASCALS_PRESSURE =     'p',
        SECONDS_TIME =         's',
        WATTS_POWER =          'w'
    };

public:
    static Units parse( const std::string& key );

    static const Units EACH;
    static const Units MASS;
    static const Units VOLUME;
    // static const TORQUE Units;
    // static const NONE Units;
    static const Units PRESSURE;
    static const Units TIME;
    static const Units POWER;

public:
    // default constructor
    Units() = delete;

    // construct by enum value
    constexpr Units( UNITS_ENUM e ): value(e) {}

    std::string abbrev() const;

    char code() const { return value; }

    std::string describe() const;

    std::string name() const;

    bool operator==( const Units& rhs ) const { return value == rhs.value; }

    bool operator==( const UNITS_ENUM other_value ) const { return value == other_value; }

    const std::string str() const { return abbrev(); }

public:
    uint8_t value;

};

}; // namespace Catalog
