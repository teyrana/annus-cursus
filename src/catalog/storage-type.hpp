//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class StorageType {
public:
    enum STORAGE_TYPE_ENUM {
        ABSTRACT = 'a',
        BULK =     'b',
        DISCRETE = 'd',
        GAS =      'g',
        LIQUID =   'l',
        LIVESTOCK ='i',
        NONE =     'n',
        PERSONNEL ='p',
        POWER =    'e'
    };

public:

    char abbrev() const { return value; }

    std::string describe() const;

    std::string name() const;

    bool operator<( const StorageType& rhs ) const { return value < rhs.value; }

    bool operator==( const StorageType& rhs ) const { return value == rhs.value; }

    bool operator==( const STORAGE_TYPE_ENUM other_value ) const { return value == other_value; }

    static StorageType parse( char code );

    static StorageType parse( const std::string& text );

    std::string str() const { return name(); }

public:
    // default constructor
    StorageType() = delete;

    // trivial constructor
    StorageType( STORAGE_TYPE_ENUM e ): value(e) {}


public:
    char value;

};

}  // namespace Catalog