//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once


#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <set>
#include <sstream>
#include <string>

#include <nlohmann/json.hpp>

namespace Catalog {

class TagSet {
public:
    std::set<std::string> tags;

public:
    TagSet() = default;
    ~TagSet() = default;

    bool contains( const std::string& tag ) const {
        return (0 < tags.count(tag));
    }

    bool empty() const { return tags.empty(); }

    size_t size() const { return tags.size(); }

    std::string str() const {
        std::ostringstream buf;

        for( const auto& tag : tags ){
            buf << tag << ',';
        }
        return buf.str();
    }

    void update( const std::string& tag ){
        tags.insert(tag);
    }

    void update( const TagSet& tag ){
        for (const auto& tag : tag.tags) {
            tags.insert(tag);
        }
    }

    void update( const nlohmann::json& blob ){
        if( blob.is_array() ){
            for( const auto& ea : blob ){
                if( ea.is_string() && (0 < ea.size()) ){
                    tags.insert(ea.get<std::string>());
                }
            }
        }
    }
};

}  // namespace Catalog
