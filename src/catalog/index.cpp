//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <fstream>
#include <filesystem>
#include <regex>

using std::filesystem::path;

#include <nlohmann/json.hpp>

#include "index.hpp"

using Catalog::Index;


static constexpr bool early_exit = false;


template<typename T>
static inline void collect_table_tags( const Catalog::EntryTable<T>& table, Catalog::TagSet& tags ){
    for( size_t table_index = 0; table_index < table.size(); ++table_index ){
        const auto& entry = table[table_index];
        tags.update(entry.tags);
    }
}

void Index::collect_tags() {
    ::collect_table_tags( building, tags );
    ::collect_table_tags( module, tags );
    ::collect_table_tags( platform, tags );
    ::collect_table_tags( process, tags );
    ::collect_table_tags( resource, tags );
    ::collect_table_tags( technology, tags);
    ::collect_table_tags( unit, tags );
}

template<typename T>
bool load_entry( const std::string& text, Catalog::EntryTable<T>& table ){

    // parse text into a json blob
    const nlohmann::json blob = nlohmann::json::parse( text, nullptr, false, true );

    // if the json blob is not valid, return false
    if( blob.is_discarded() ){
        std::cerr << "  !! Fatal -- Failed to parse JSON:!?: " << text << std::endl;
        return false;
    }

    const std::string name = blob.value("name","");
    if( name.empty() ){
        std::cerr << "<<!! Entry is missing required field: 'name' \n";
        return false;
    }

    const std::string key = blob.value("key","");
    if( key.empty() ){
        std::cerr << "<<!! Entry is missing required field: 'key' \n";
        return false;
    }

    // needs to be mutable -- we're not done loading this entry, yet
    T& entry = table.emplace( name, key );

    // // DEBUG
    // const size_t debug_entry_index = 216;
    // if( debug_entry_index == entry.index ){
    //     std::cerr << "            :1: tags: " << entry.tags.size() << std::endl;
    // }
    // // DEBUG

    // if there is a 'super' entry, load that first
    if( blob.contains("super") ){
        const std::string super_key = blob["super"];
        if( table.contains(super_key) ){
            const T& parent = * table.get(super_key);
            entry.update( parent );
        } else {
            std::cerr << "<<!! Entry references non-existent super entry: " << super_key << std::endl;
            return false;
        }
    }

    // update with the actual json contents:
    entry.update( blob );

    return true;
}

template<typename T>
bool load_entry_table( const path& entry_data_file_path, Catalog::EntryTable<T>& table ){
    if( ! std::filesystem::exists( entry_data_file_path ) ){
        std::cerr << "  !! Fatal -- Data directory does not exist: " << entry_data_file_path << std::endl;
        return false;
    }

    std::ifstream entry_data_file( entry_data_file_path );
    if( ! entry_data_file.is_open() ){
        std::cerr << "  !! Fatal -- Failed to open data file: " << entry_data_file_path << std::endl;
        return false;
    }

    size_t depth = 0;
    size_t line_count = 0;
    size_t entry_count = 0;
    std::string line;
    std::ostringstream buffer;
    while( std::getline( entry_data_file, line ) ){
        ++line_count;

        if( line.empty() ){
            continue;
        }else if (line.length() < 1) {
            continue;
        }

        // remove any leading whitespace:
        line.erase( 0, line.find_first_not_of(" \t\n\r\f\v") );

        // remove any trailing whitespace:
        line.erase( line.find_last_not_of(" \t\n\r\f\v") + 1 );

        if( line.starts_with("//") ){
            continue;
        }

        // count the number of open brackets ('{') in this line: 
        const size_t count_open_brace = std::count( line.begin(), line.end(), '{' );
        // count the number of close brackets ('}') in this line:
        const size_t count_close_brace = std::count( line.begin(), line.end(), '}' );
        // calculate the current depth of the JSON object:
        const int cur_depth = depth + count_open_brace - count_close_brace;

        if( 0 == depth ){
            if( 0 == cur_depth ){
                // start (and finish) a simple-entry
                // console.log(`:[${lineCount.toString().padStart(3)}]:{}`)
                ++entry_count;
                buffer << line;
            }else if( 0 < count_open_brace ){
                // start a multi-line-entry
                // console.log(`:[${lineCount.toString().padStart(3)}]:{`)
                buffer << line;
            }else{
                // console.log(`:[${lineCount.toString().padStart(3)}]:???`)
            }
        }else{
            if( 0 == cur_depth ){
                // finish a multi-line-entry
                // console.log(`:[${lineCount.toString().padStart(3)}]: }`)
                ++entry_count;
                buffer << line;
            }else{
                // console.log(`:[${lineCount.toString().padStart(3)}]:    :`)
                buffer << line;
            }
        }
        depth = cur_depth;

        if( (0==depth) && (0<buffer.tellp()) ){
            std::string entry_text = buffer.str();

            // lets be permissive about these syntax options:
            // (1) erase any commas directly before a closing brace:
            entry_text = std::regex_replace( entry_text, std::regex(",}"), "}" );
            // (2) erase any trailing commas:
            line.erase( line.find_last_not_of(",\n\r") + 1 );

            entry_text = std::regex_replace( entry_text, std::regex("},$"), "}" );

            // // DEBUG
            // fprintf(stderr, "        Loading Entry @  [%3lu][%3lu]: %s\n", line_count, entry_count, entry_text.c_str() );

            if( ! load_entry( entry_text, table ) ){
                fprintf(stderr, "  <<!! Invalid <%s> Entry!!\n", T::entry_type_name().c_str() );
                fprintf(stderr, "       :Line:  [%3lu]: %s\n", line_count, line.c_str() );
                fprintf(stderr, "       :buffer:[%3lu]: %s\n", entry_count, buffer.str().c_str() );

                if(early_exit){
                    return false;
                }
            }

            // clear the contents of the accumulation buffer:
            buffer.str("");
        }
    }
    entry_data_file.close();

    std::cout << "        <<< Loaded: " << line_count << " lines into " << entry_count << " entries." << std::endl;

    table.loaded = true;
    return table.loaded;
}


bool Index::load( const path & data_path ){
    if( ! std::filesystem::exists( data_path ) ){
        std::cerr << "!! Fatal -- Data directory does not exist: " << data_path << std::endl;
        return false;
    }

    // std::cerr << "    ::Path Exists: " << std::filesystem::absolute(data_path) << std::endl;

    std::cout << "  >> 1: Loading Buildings..." << std::endl;
    const path buildings_file_path = data_path / "buildings.json";
    load_entry_table( buildings_file_path, building );
    if( not building.loaded ){
        std::cerr << "!! Fatal -- Failed to load buildings from: " << buildings_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 2: Load Modules..." << std::endl;
    const path modules_file_path = data_path / "modules.json";
    load_entry_table( modules_file_path, module );
    if( not module.loaded ){
        std::cerr << "!! Fatal -- Failed to load modules from: " << modules_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 3: Load Platforms..." << std::endl;
    const path platforms_file_path = data_path / "platforms.json";
    load_entry_table( platforms_file_path, platform );
    if( not platform.loaded ){
        std::cerr << "!! Fatal -- Failed to load platforms from: " << platforms_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 4: Loading Processes... " << std::endl;
    const path processes_file_path = data_path / "processes.json";
    load_entry_table( processes_file_path, process );
    if( not process.loaded ){
        std::cerr << "!! Fatal -- Failed to load processes from: " << processes_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 5: Loading Resources..." << std::endl;
    const path resources_file_path = data_path / "resources.json";
    load_entry_table( resources_file_path, resource );
    if( not resource.loaded ){
        std::cerr << "!! Fatal -- Failed to load resources from: " << resources_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 6: Loading Technologies..." << std::endl;
    const path technologies_file_path = data_path / "technologies.json";
    load_entry_table( technologies_file_path, technology );
    if( not technology.loaded ){
        std::cerr << "!! Fatal -- Failed to load technologies from: " << technologies_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    std::cout << "  >> 7: Loading Units..." << std::endl;
    const path units_file_path = data_path / "units.json";
    load_entry_table( units_file_path, unit );
    if( not unit.loaded ){
        std::cerr << "!! Fatal -- Failed to load units from: " << units_file_path << std::endl;
        if (early_exit) {
            return false;
        }
    }

    // { // verify linkages
        // -- i.e. that keys listed in one type refer to an actual entries listed other types

    //   // buildings
    //   catalog.building.valid = catalog.building.values().every(building => {
    //     return building.verify(catalog.module)
    //   })

    //   // modules
    //   catalog.module.valid = catalog.module.values().every(module => {
    //     return module.verify(catalog.process, catalog.resource)
    //   })

    //   // platforms
    //   catalog.platform.valid = catalog.platform.values().every(platform => {
    //     return platform.verify(catalog.module)
    //   })

    //   // resources have no links

    //   // technlogies
    //   catalog.technology.valid = catalog.technology.values().every(technology => {
    //     return technology.verify( catalog.technology )
    //   })

    //   // units
    //   catalog.unit.valid = catalog.unit.values().every( unit => {
    //     return unit.verify( catalog.module )
    //   })

    // }

    // std::cout << "  >> 7: Loading Units..." << std::endl;

    return (   building.loaded
            && module.loaded
            && platform.loaded
            && process.loaded
            && resource.loaded
            && technology.loaded
            && unit.loaded
    );
}
