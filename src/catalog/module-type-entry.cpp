// Copyright (C) 2023, MIT License, Daniel Williams

#include <sstream>

#include "matchers.hpp"
#include "module-type-entry.hpp"
#include "parsers.hpp"
#include "units.hpp"

using Catalog::ModuleTypeEntry;


std::string ModuleTypeEntry::str() const { 
    std::ostringstream buf; 
    constexpr char indent[] = "        ";

    buf << indent << header_string() << '\n';

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]\n";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description  << '\n';
    }
    
    if( ! super.empty() ){
        buf << indent << "    - parent: " << super  << '\n';
    }

    buf << indent << "    - mass:   " << std::fixed << std::setw(4) << std::setprecision(2) << mass_ << ' ' << mass_units.describe() << '\n';
    
    buf << indent << "    - mount:  " << mount_.name() << '\n';

    buf << indent << "    - volume: " << std::fixed << std::setw(4) << std::setprecision(2) << volume_ << ' ' << volume_units.describe() << '\n';

    if( 0 < process_.size() ){
        buf << indent << "    - process: \n";
        for ( const auto& ea : process_ ){
            buf << indent << "         - " << ea << '\n';
        }
    }

    buf << indent << "    - storage: " << storage_.size() << '\n';
    if( 0 < storage_.size() ){
        for ( const auto& pair : storage_ ){
            buf << indent << "         - " << pair.first.name() << ": " << std::fixed << std::setw(4) << std::setprecision(2) << pair.second << '\n';
        }
    }

    return buf.str();
}

void ModuleTypeEntry::update( const nlohmann::json& blob ){
    if( not blob.is_object() ){
        return;
    }

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if("super" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);


        }else if( "mass" == key){
            mass_ = parse_as_float(value, mass_);
        }else if( "mount" == key){
            mount_ = MountType::parse(value);

        }else if( match_start(key, "vol") ){
            volume_ = parse_as_float(value, volume_);

        }else if( match_start(key, "proc")){
            for( const auto& ea : value ){
                process_.emplace( ea.get<std::string>() );
            }
        }else if( match_start(key, "stor") ){
            for( const auto& [r,q] : value.items() ){
                storage_.emplace( StorageType::parse(r), q.get<float>() );
            }
        }else{
            std::cerr << "!!!! Could not load JSON key: " << key << " into class: <" << entry_type_name() << ">: " << this->key << std::endl;
            return;
        }
    }
}


void ModuleTypeEntry::update( const ModuleTypeEntry& parent ){
    super = parent.key;
    mass_ = parent.mass_;
    description = parent.description;
    process_ = {parent.process_};
    mount_ = parent.mount_;
    storage_ = {parent.storage_};
    tags.update(parent.tags);
    volume_ = parent.volume_;
}

bool ModuleTypeEntry::valid() const {
    return true;
}
