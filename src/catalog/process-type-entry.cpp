//Copyright (c) 2023, MIT License, Daniel Williams

#include <iostream>
#include <string>
#include <sstream>


#include <nlohmann/json.hpp>

#include "matchers.hpp"
#include "process-type-entry.hpp"
#include "tag-set.hpp"

using Catalog::ProcessTypeEntry;


std::string ProcessTypeEntry::str() const {
    std::ostringstream buf;

    constexpr char indent[] = "        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description  << '\n';
    }
    
    if( ! super.empty() ){
        buf << indent << "    - parent: " << super  << '\n';
    }

    if( 0 < io_.size() ){
        buf << indent << "    - io: \n";
        for ( const auto& [rsc,qty] : io_ ){
            buf << indent << "         - " << rsc << ": " << qty << '\n';
        }
    }
    
    return buf.str();
}

void ProcessTypeEntry::update( const nlohmann::json& blob ){
    if( not blob.is_object() ){
        return;
    }

    for (const auto& [key, value] : blob.items()) {
        if ("key" == key) {
            // pass -- already loaded
        }else if ("index" == key) {
            // pass -- ignore
        }else if ("name" == key) {
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            super = value.get<std::string>();

        } else if ("io" == key) {
            if( value.is_object() ){
                for( const auto& ea : value.items() ){
                    io_.emplace( ea.key(), ea.value().get<int>() );
                }
            }else{
                std::cerr << "!!!! Expected JSON key: 'io' for class: <" << entry_type_name() << ">... but key was not a resource -> quantity map?!" << std::endl;
            }
        }else{
            std::cerr << "!!!! Could not load JSON key: " << key << " into class: <" << entry_type_name() << ">: " << this->key << std::endl;
            return;
        }
    }
}

void ProcessTypeEntry::update( const ProcessTypeEntry& parent ){
    description = parent.description;
    tags.update(parent.tags);
}

bool ProcessTypeEntry::valid() const {
    return static_cast<bool>( 0 < io_.size());
}

