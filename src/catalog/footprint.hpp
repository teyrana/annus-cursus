// Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class FootPrint {
public:
    uint32_t x;
    uint32_t y;

public:
    FootPrint(uint32_t _x, uint32_t _y)
        :x(_x), y(_y) 
    {}

    FootPrint( const FootPrint& other )
        : x(other.x), y(other.y)
    {}

public:
    float height() const { return y; }
    float width() const { return x; }
    
    float area() const { return x * y; }

    bool operator==( const FootPrint& other ) const { 
        return ((x == other.x) && (y == other.y));
    }

    void operator=( const FootPrint& other ) { 
        x = other.x;
        y = other.y;
    }

    std::string str() const {
        return std::string("[") + std::to_string(x) + ", " + std::to_string(y) + "]";
    }

};

} // namespace Catalog

