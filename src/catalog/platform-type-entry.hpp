//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include "base-type-entry.hpp"
#include "bounding-box.hpp"
#include "entry-table.hpp"
#include "mobility-type.hpp"
#include "mount-type.hpp"
#include "tag-set.hpp"

namespace Catalog {

class PlatformTypeEntry : public BaseTypeEntry<PlatformTypeEntry> {
public:
    PlatformTypeEntry()
        : PlatformTypeEntry("Root Resource", 0, "root")
    {}

    PlatformTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
        , armor_(0)
        // this 10x10x10 === 1 map tile is the default building & vehicle size
        , bounds_( 10, 10, 10 )
        , mass_(0)
        , mobility_(MobilityType::STATIC)
        // , modules_available_()
        // , mounts_available_()
        , volume_(0)
    {}

    ~PlatformTypeEntry() = default;

    float armor() const { return armor_; }

    static std::string entry_type_name(){ return "PlatformTypeEntry";}

    int hitpoints() const { return hitpoints_; }

    float mass() const { return mass_; }

    MobilityType mobility() const { return mobility_; }

    std::set<std::string> modules() const { return modules_available_; }

    std::map<MountType,int> mounts() const { return mounts_available_; }

    float volume() const { return volume_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const PlatformTypeEntry& parent );

    template< typename module_table_t>
    bool verify( module_table_t modules ) const;

    bool valid() const;


private:

    float armor_;

    // this 10x10x10 === 1 map tile is the default building & vehicle size
    // needed for buildings -> should define a canonical tile size -> 10m x 10m?
    // default size is this 1x1 tile === (10m x 10m)
    BoundingBox bounds_ = BoundingBox( 10, 10, 10 );


    // base hitpoints of platform
    int hitpoints_;

    // mass of platform itself
    int mass_;

    MobilityType mobility_;

    std::set<std::string> modules_available_;

    // available external module mounts
    std::map<MountType,int> mounts_available_;


    // available volume for internal modules
    int volume_;

};

template< typename module_table_t>
bool PlatformTypeEntry::verify( module_table_t modules ) const {

    // check modules
    for( const auto& mod : modules_available_ ){
        if( ! modules.contains( mod ) ){
            std::cout << "    !! could not find module: " << mod << " in platform: " << key << '\n';
            return false;
        }
    }

    return true;
}

} // namespace Catalog
