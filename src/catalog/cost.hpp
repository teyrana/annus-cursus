// Copyright (c) 2023, MIT License, Daniel Williams

#include <map>
#include <sstream>
#include <string>

namespace Catalog {

class Cost : public std::map<std::string,float> {

    Cost(){
        emplace("time", 0.0);
    }

    std::string str() const {
        std::ostringstream buf;
        for (const auto& [rsc, qty] : *this) {
            buf << rsc << ':' << qty << ',  ';
        }
        return buf.str();
    }
    
};

} // namespace Catalog
