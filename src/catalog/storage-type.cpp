//Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <string>

#include "matchers.hpp"
#include "storage-type.hpp"
using Catalog::StorageType;


std::string StorageType::describe() const {
    switch( value ){
        default: return "(Unknown storage type)";
        case ABSTRACT: return "For items which have no storage limit: money, prestige, or experience";
        case BULK: return "For solid resources in continuous, indeterminate quantities -- sand or coal";
        case DISCRETE: return "For resources in discrete sizes: i.e. a sheep, a missile";
        case GAS: return "Expands to fill available volume";
        case LIQUID: return "Liquid; like water; settles into any shaped, but has a fixed volume";
        case LIVESTOCK: return "Living creatures, such as cows or rabbits";
        case NONE: return "Cannot be stored";
        case PERSONNEL: return "People (i.e. seats)";
        case POWER: return "Specifically electrical power. Requires special equipment, such as batteries";
    }
}

std::string StorageType::name() const {
    switch(value){
        case ABSTRACT: return "ABSTRACT";
        case BULK: return "BULK";
        case DISCRETE: return "DISCRETE";
        case GAS: return "GAS";
        case LIQUID: return "LIQUID";
        case LIVESTOCK: return "LIVESTOCK";
        case NONE: return "NONE";
        case PERSONNEL: return "PERSONNEL";
        case POWER: return "POWER";
        default: return "NONE";
    }
}

StorageType StorageType::parse( char code ){
    switch( code ){
        default: return NONE;
        case ABSTRACT: return ABSTRACT;
        case BULK: return BULK;
        case DISCRETE: return DISCRETE;
        case GAS: return GAS;
        case LIQUID: return LIQUID;
        case LIVESTOCK: return LIVESTOCK;
        case NONE: return NONE;
        case PERSONNEL: return PERSONNEL;
        case POWER: return POWER;
    }
}

StorageType StorageType::parse( const std::string& text ){
    // transform to lower case:
    std::string name{text};
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

    if( 0 == name.length() ){
        return StorageType{STORAGE_TYPE_ENUM::NONE};
    }else if( 1 == name.length() ){
        return parse( name[0] );
    }
    if( "abstract" == name ){
        return StorageType{ABSTRACT};
    } else if( "bulk" == name ){
        return StorageType{BULK};
    } else if( match_any(name, {"discrete", "each", "count" }) ){
        return StorageType{DISCRETE};
    } else if( "gas" == name ){
        return StorageType{GAS};
    } else if( "liquid" == name ){
        return StorageType{LIQUID};
    } else if( match_any(name, {"livestock", "animal", "creature", "farm" }) ){
        return StorageType{LIVESTOCK};
    } else if( "none" == name ){
        return StorageType{NONE};

    } else if( match_start( name, "person") ){
        return StorageType{PERSONNEL};

    } else if( "power" == name ){
        return StorageType{POWER};
    }
    return StorageType::NONE;
}



