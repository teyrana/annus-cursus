//Copyright (c) 2023, MIT License, Daniel Williams

#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>

#include "matchers.hpp"
#include "parsers.hpp"
#include "unit-type-entry.hpp"

using Catalog::UnitTypeEntry;


std::string UnitTypeEntry::str() const {
    std::ostringstream buf;
    constexpr char indent[] = "        ";

    buf << indent << header_string();

    if( 0 < tags.size() ){
        buf << indent << "    - tags:[ " << tags.str() << " ]";
    }

    if( not description.empty() ){
        buf << indent << "    - " << description  << '\n';
    }

    if( ! super.empty() ){
        buf << indent << "    - parent: " << super  << '\n';
    }

    buf << indent << "    - mass: " << mass_ << '\n';
    buf << indent << "    - volume: " << volume_ << '\n';


    if (0 < modules_.size()) {
        buf << "    - Modules: ";
        for( const auto& [mod,qty] : modules_ ){
            buf << indent << "           - " << mod << "x" << qty << " ";
        }
    }

    buf << indent << "    - platform: " << platform_ << '\n';

    return buf.str();
}

void UnitTypeEntry::update( const nlohmann::json& blob ){
    if( ! blob.is_object() ){
        return;
    }

    for( const auto& [key,value] : blob.items() ){
        if("key" == key){
            // pass -- already loaded
        }else if("index" == key){
            // pass -- ignore
        }else if("name" == key){
            // pass -- already loaded
        }else if( match_start(key, "desc")){
            description = value.get<std::string>();
        }else if( match_start( key, "tag") ){
            tags.update(value);
        }else if("super" == key){
            super = value.get<std::string>();

        }else if("mass" == key){
            mass_ = parse_as_float( value, mass_ );
        }else if("volume" == key){
            volume_ = parse_as_float( value, volume_ );

        }else if( match_start( key, "mod") ){
            if( value.is_object() ){
                for( const auto& [mod,qty] : value.items() ){
                    modules_.emplace( mod, qty.get<int>() );
                }
            }
        } else if( match_start( key, "plat" )){
            platform_ = value.get<std::string>();
        }else{
              std::cerr << "!!!! Could not load JSON key: " << key << " into class: <" << entry_type_name() << ">: " << this->key << std::endl;
            return;
        }
    }
}


void UnitTypeEntry::update( const UnitTypeEntry& parent ){
    description = parent.description;
    tags.update( parent.tags );


    mass_ = parent.mass_;
    volume_ = parent.volume_;

    platform_ = parent.platform_;
    modules_ = { parent.modules_ };
}


bool UnitTypeEntry::valid() const {
    return true;
}

