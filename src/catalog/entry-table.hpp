//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <limits>
#include <map> 
#include <string>
#include <vector>


namespace Catalog {

template< typename entry_type_t >
class EntryTable {
public:
    EntryTable() = default;

    ~EntryTable() = default;

    // emplace by forwarding the arguments to the templated type constructor:
    entry_type_t& emplace( const std::string& _name, const std::string& _key ){
        const size_t index = by_index.size();
        entry_type_t& entry = by_index.emplace_back( _name, index, _key );
        by_key[entry.key] = entry.index;
        return entry;
    }

    // void add( entry_type_t* entry ){
    //     map_by_key[entry->key()] = entry;
    //     index.push_back(entry);
    //}

    bool contains( const std::string& key ) const {
        // C++20 
        return by_key.contains(key);
    }

    size_t lookup( const std::string& lookup ) const { 
        if( by_key.contains(lookup) ){
            return by_key.at(lookup);
        }
        return std::numeric_limits<size_t>::max();
    }

    entry_type_t* get( const std::string& lookup ) {
        if( by_key.contains(lookup) ){
            const size_t index = by_key.at(lookup);
            if( index < by_index.size() ){
                return &by_index[index];
            }
        }
        return nullptr;
    }

    const entry_type_t* get(size_t index) const { 
        return &by_index[index]; }

    const entry_type_t& operator[](size_t index) const {
        return by_index[index]; }

    size_t size() const { return by_index.size(); }

public:
    bool loaded = false;

private:
    std::map<std::string,size_t> by_key;
    std::vector<entry_type_t> by_index;

};

}  // namespace Catalog