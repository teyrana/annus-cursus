//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <vector>
#include <map>

#include <nlohmann/json.hpp>

#include "base-type-entry.hpp"
#include "tag-set.hpp"

namespace Catalog {

class ProcessTypeEntry : public BaseTypeEntry<ProcessTypeEntry> {
public:
    ProcessTypeEntry()
        : ProcessTypeEntry("Root Process", 0, "root")
    {}

    ProcessTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
    {}

    static std::string entry_type_name() { return "ProcessTypeEntry"; }

    std::map<std::string, float> io() const { return io_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const ProcessTypeEntry& parent );

    template< typename resource_table_t >
    bool validate( resource_table_t rtable ) const;

    bool valid() const;


private:
    /// \brief maps a resource id to a quantity
    std::map<std::string, float> io_;

};

}  // namespace Catalog

