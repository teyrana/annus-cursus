// Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {


class MountType {
public:
    enum MOUNT_TYPE_ENUM {
        INTERNAL    = 'i',
        HAND        = 'h',
        STATIONARY  = 's',
        FIXED       = 'f'
    };

    MOUNT_TYPE_ENUM value;

public:
    MountType() = delete;

    MountType( MOUNT_TYPE_ENUM e ): value(e) {}

    MountType( const MountType& other ) : value(other.value) {}

    char code() const { return value; }

    std::string describe() const;

    std::string name() const; 

    // include this so we can use std::map<MountType, ...>
    bool operator<( const MountType& other ) const { return value < other.value; }

    void operator=( MountType other ){  value = other.value; }

    bool operator==( const MountType& rhs ) const { return value == rhs.value; }

    bool operator==( const MOUNT_TYPE_ENUM other_value ) const { return value == other_value; }

    static MountType parse( const std::string& text );

};

} // namespace Catalog

