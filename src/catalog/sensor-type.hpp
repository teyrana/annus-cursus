// Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class SensorType {
public:
    enum SENSOR_TYPE_ENUM {
        UNKNOWN =    '?',
        VISUAL =     'V',
        INFRARED =   'I',
        AIRBORNE =   'A',
        RADAR =      'R',
        ELECTRONIC = 'E'
    };
    SENSOR_TYPE_ENUM value;

public:
    SensorType() = delete;

    SensorType( SENSOR_TYPE_ENUM e )
        : value(e)
    {}

    SensorType( const SensorType& other )
        : value(other.value)
    {}

    char code() const { return value; }

    std::string describe() const;

    std::string name() const; 

    bool operator==( const SensorType& rhs ) const { return value == rhs.value; }

    bool operator==( const SENSOR_TYPE_ENUM other_value ) const { return value == other_value; }

    static SensorType parse( const std::string& text );

};


}  // namespace Catalog

