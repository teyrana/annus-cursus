// Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <string>

#include "mobility-type.hpp"
using Catalog::MobilityType;


std::string MobilityType::describe() const {
    switch(value){
      default:
      case UNKNOWN: 
          return "Unknown";
      case STATIC:
          return "Static";
      case INFANTRY:
          return "Infantry";
      case WHEELED:
          return "Wheeled Vehicles";
      case TRACKED:
          return "Tank Treads";
      case RAIL:
          return "Rail Train";
      case MECH:
          return "Mechanized Walker -- similiar to Infantry, but heavier";
      case HELICOPTER:
          return "Helicopter";
      case AIRPLANE:
          return "Airplane";
      case BOAT:
          return "Riverine Boat -- shallow water; may land/dock at beaches";
      case SHIP:
          return "Ocean Ship -- requires deep water; requires port facilities";
      case SUBMARINE:
          return "Submarine -- requires deep water; even when surfaced";
    }
}



std::string MobilityType::name() const {
    switch(value){
      default:
      case UNKNOWN: 
          return "Unknown";
      case STATIC:
          return "Static";
      case INFANTRY:
          return "Infantry";
      case WHEELED:
          return "Wheeled";
      case TRACKED:
          return "Tracked";
      case RAIL:
          return "Train";
      case MECH:
          return "Mecha";
      case HELICOPTER:
          return "Helicopter";
      case AIRPLANE:
          return "Airplane";
      case BOAT:
          return "Boat";
      case SHIP:
          return "Ship";
      case SUBMARINE:
          return "Submarine";
    }
}


MobilityType MobilityType::parse( const std::string& text ){
  if( text.empty() ){
        return MobilityType::UNKNOWN;
    }else if( 1 == text.size() ){
        switch( tolower(text[0]) ){
          default:
          case MobilityType::UNKNOWN:
              return MobilityType::UNKNOWN;
          case MobilityType::STATIC:
              return MobilityType::STATIC;
          case MobilityType::INFANTRY:
              return MobilityType::INFANTRY;
          case MobilityType::WHEELED:
              return MobilityType::WHEELED;
          case MobilityType::TRACKED:
              return MobilityType::TRACKED;
          case MobilityType::RAIL:
              return MobilityType::RAIL;
          case MobilityType::MECH:
              return MobilityType::MECH;
          case MobilityType::HELICOPTER:
              return MobilityType::HELICOPTER;
          case MobilityType::AIRPLANE:
              return MobilityType::AIRPLANE;
          case MobilityType::BOAT:
              return MobilityType::BOAT;
          case MobilityType::SHIP:
              return MobilityType::SHIP;
          case MobilityType::SUBMARINE:
              return MobilityType::SUBMARINE;
        }
    }

    // convert parameter string to lower case
    std::string lowkey = text;
    std::transform(lowkey.begin(), lowkey.end(), lowkey.begin(), ::tolower);

    if( lowkey == "static" ){
        return MobilityType::STATIC;
    }else if( lowkey == "infantry" ){
        return MobilityType::INFANTRY;
    }else if( lowkey == "wheel" ){
        return MobilityType::WHEELED;
    }else if( lowkey == "track" ){
        return MobilityType::TRACKED;
    }else if( lowkey == "rail" ){
        return MobilityType::RAIL;
    }else if( lowkey == "mech" ){
        return MobilityType::MECH;
    }else if( lowkey == "helo" ){
        return MobilityType::HELICOPTER;
    }else if( lowkey == "airplane" ){
        return MobilityType::AIRPLANE;
    }else if( lowkey == "river" ){
        return MobilityType::BOAT;
    }else if( lowkey == "ship" ){
        return MobilityType::SHIP;
    }else if( lowkey == "sub" ){
        return MobilityType::SUBMARINE;
    }

    return MobilityType::UNKNOWN;
}