//Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <string>
#include <cstdint>

#include "matchers.hpp"
#include "units.hpp"

using Catalog::Units;


const Units Units::EACH = UNITS_ENUM::EACH_COUNT;
const Units Units::MASS = UNITS_ENUM::KILOGRAMS_MASS;
const Units Units::VOLUME = UNITS_ENUM::LITERS_VOLUME;
// const Units Units::NEWTON_METERS_TORQUE = Units( UNITS_ENUM::
// const Units Units::NONE = Units( UNITS_ENUM::
const Units Units::PRESSURE = UNITS_ENUM::PASCALS_PRESSURE;
const Units Units::TIME = UNITS_ENUM::SECONDS_TIME;
const Units Units::POWER = UNITS_ENUM::WATTS_POWER;


std::string Units::abbrev() const {
    switch(value){
        case Units::EACH_COUNT: return "ct";
        case Units::NONE: return "";
        case Units::KILOGRAMS_MASS: return "kg";
        case Units::LITERS_VOLUME: return "L";
        case Units::NEWTON_METERS_TORQUE: return "Nm";
        case Units::PASCALS_PRESSURE: return "Pa";
        case Units::SECONDS_TIME: return "s";
        case Units::WATTS_POWER: return "W";
        default: return "kg";
    }
}


std::string Units::name() const {
    switch(value){
        case Units::EACH_COUNT: return "Count";
        case Units::KILOGRAMS_MASS: return "Mass";
        case Units::LITERS_VOLUME: return "Volume";
        case Units::NEWTON_METERS_TORQUE: return "Torque";
        case Units::NONE: return "Empty";
        case Units::PASCALS_PRESSURE: return "Pressure";
        case Units::SECONDS_TIME: return "Time";
        case Units::WATTS_POWER: return "Power";
        default: return "Mass";
    }
}

std::string Units::describe() const { 
    return abbrev() + " " + name();
}

Units Units::parse( const std::string& key ){
    if( 0 == key.length() ){
        return Units{NONE};
    }

    if( 1 == key.length() ){
        switch( tolower(key[0]) ){
            case EACH_COUNT: return Units{EACH_COUNT};
            case KILOGRAMS_MASS: return Units{KILOGRAMS_MASS};
            case LITERS_VOLUME: return Units{LITERS_VOLUME};
            case NEWTON_METERS_TORQUE: return Units{NEWTON_METERS_TORQUE};
            default:
            case NONE: return Units{NONE};
            case PASCALS_PRESSURE: return Units{PASCALS_PRESSURE};
            case SECONDS_TIME: return Units{SECONDS_TIME};
            case WATTS_POWER: return Units{WATTS_POWER};
        }
    }

    // otherwise match the longer aliases:
    // mostly representing units:

    // convert parameter string to lower case
    std::string lowkey = key;
    std::transform(lowkey.begin(), lowkey.end(), lowkey.begin(), ::tolower);

    if( match_any( lowkey, {"count", "each"} )){
      return Units{EACH_COUNT};
    }else if(("kg" == lowkey)){
      return Units{KILOGRAMS_MASS};
    }else if("nm" == lowkey){
      return Units{NEWTON_METERS_TORQUE};
    }else if(("pa" == lowkey)){
      return Units{PASCALS_PRESSURE};
    }else if(("sec" == lowkey)){
      return Units{SECONDS_TIME};
    }

    return Units{NONE};
}
