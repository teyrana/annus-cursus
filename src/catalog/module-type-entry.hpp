//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <iostream>
#include <iomanip>
#include <map>
#include <set>
#include <string>

#include <nlohmann/json.hpp>

#include "base-type-entry.hpp"
#include "mount-type.hpp"
#include "storage-type.hpp"
#include "tag-set.hpp"
#include "units.hpp"

#include "tag-set.hpp"


namespace Catalog {

class ModuleTypeEntry : public BaseTypeEntry<ModuleTypeEntry> {
public:
    ModuleTypeEntry()
        : ModuleTypeEntry("Root Module", 0, "root")
    {}

    ModuleTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
        , mass_(1000.0)
        , mount_(MountType::INTERNAL)
        , volume_(1000.0)
    {}

    ~ModuleTypeEntry() = default;

    static std::string entry_type_name(){ return "ModuleTypeEntry";}

    float mass() const { return mass_; }
    float volume() const { return volume_; }
    Catalog::MountType mount() const { return mount_; }

    std::set<std::string> process() const { return process_; }

    std::map<StorageType, double> storage() const { return storage_; }

    void update( const nlohmann::json& blob );

    void update( const ModuleTypeEntry& parent );

    template<typename proc_table_t>
    inline bool verify( proc_table_t proc_table ) const;

    std::string str() const;

    bool valid() const;


public:
    // Mass of the module _itself_
    // - units === kilograms
    double mass_;
    static constexpr Catalog::Units mass_units = Catalog::Units::UNITS_ENUM::KILOGRAMS_MASS;

    Catalog::MountType mount_;
    
    // Volume of the module _itself_
    // - units === L (liters, litres)
    double volume_;
    static constexpr Catalog::Units volume_units = Catalog::Units::UNITS_ENUM::LITERS_VOLUME;

    // list of resources this module can run:
    std::set<std::string> process_;

    // storage-type-key -> quantity
    std::map<Catalog::StorageType, double> storage_;

};

template<typename proc_table_t>
inline bool ModuleTypeEntry::verify( proc_table_t proc_table ) const {

    // .1. module => process
    for( const auto& proc_key : process_ ){
        if( ! proc_table.contains(proc_key) ){
            std::cerr << "    !!! @<" << entry_type_name() << ">: " << std::setw(32) << key << "  ... could not find process: " << proc_key << std::endl;
            return false;
        }
    }

    return true;
}



} // namespace Catalog
