//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <iostream>
#include <string>

#include <nlohmann/json.hpp>

#include "base-type-entry.hpp"
#include "footprint.hpp"
#include "mount-type.hpp"
#include "storage-type.hpp"
#include "tag-set.hpp"
#include "units.hpp"

#include "tag-set.hpp"


namespace Catalog {

class BuildingTypeEntry : public BaseTypeEntry<BuildingTypeEntry> {
public:
    BuildingTypeEntry()
        : BuildingTypeEntry("Root Building", 0, "root")
    {}

    BuildingTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
        , armor_(0.0)
        , footprint_(10,10)
        , hitpoints_(100)
        // , modules_compatable_( n/a )
        // , mounts_available_( n/a )
        , volume_(1000.0)
    {}

    ~BuildingTypeEntry() = default;

    static std::string entry_type_name(){ return "BuildingTypeEntry";}

    float armor() const { return armor_; }
    FootPrint footprint() const { return footprint_; }
    std::set<std::string> modules() const { return modules_available_; }
    std::vector<MountType> mounts() const { return mounts_available_; }
    int hitpoints() const { return hitpoints_; }
    float volume() const { return volume_; }

    // StorageType& storage() { return storage_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const BuildingTypeEntry& parent );

    bool valid() const;

    template<typename T>
    inline bool verify( T modules );

public:
    const static BuildingTypeEntry RootResource;

    float armor_;
    FootPrint footprint_;
    int hitpoints_;
    std::set<std::string> modules_available_;
    std::vector<MountType> mounts_available_;
    float volume_;


    // float mass_; // units = kilograms
    // StorageType storage_;
    // Units units_;
    // float volume_;  // units == L (liters, litres)

};

template<typename T>
inline bool BuildingTypeEntry::verify(  T modules )
                        // modules: EntryTable<ModuleType>): boolean
{
    for( auto& mod : modules ){
        if( not modules.contains(mod.key) ){
            std::cerr << "    !! could not find module: " << key << " from building: " << key << std::endl;
            return false;
        }
    }

    return true;
}


}  // namespace Catalog
