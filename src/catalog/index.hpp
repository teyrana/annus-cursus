//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include "entry-table.hpp"
#include "building-type-entry.hpp"
#include "module-type-entry.hpp"
#include "platform-type-entry.hpp"
#include "process-type-entry.hpp"
#include "resource-type-entry.hpp"
#include "tag-set.hpp"
#include "technology-type-entry.hpp"
#include "unit-type-entry.hpp"

namespace Catalog {

class Index {
public:
    Index() = default;
    ~Index() = default;

    bool load( const std::filesystem::path& data_path );

    void collect_tags();

    EntryTable<BuildingTypeEntry> building;
    EntryTable<ModuleTypeEntry> module;
    EntryTable<PlatformTypeEntry> platform;
    EntryTable<ProcessTypeEntry> process;
    EntryTable<ResourceTypeEntry> resource;
    EntryTable<TechnologyTypeEntry> technology;
    EntryTable<UnitTypeEntry> unit;

    TagSet tags;

};


} // namespace Catalog
