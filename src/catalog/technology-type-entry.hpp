//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <set>

#include "base-type-entry.hpp"
#include "entry-table.hpp"
#include "tag-set.hpp"

namespace Catalog {

class TechnologyTypeEntry : public BaseTypeEntry<TechnologyTypeEntry> {
public:

    TechnologyTypeEntry()
        : BaseTypeEntry("Root Technology", 0, "root")
    {}

    TechnologyTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
    {}

    ~TechnologyTypeEntry() = default;

    static std::string entry_type_name(){ return "TechnologyType";}

    const static TechnologyTypeEntry RootTechnology;

    std::set<std::string> requirements() const { return requires_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const TechnologyTypeEntry& parent );

    bool valid() const;

    bool verify( const EntryTable<TechnologyTypeEntry>& table ) const;


private:
    std::set<std::string> requires_;

};


}  // namespace Catalog