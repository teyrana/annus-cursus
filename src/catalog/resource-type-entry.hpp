//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <nlohmann/json.hpp>

#include "base-type-entry.hpp"
#include "storage-type.hpp"
#include "tag-set.hpp"
#include "units.hpp"


namespace Catalog {

class ResourceTypeEntry : public BaseTypeEntry<ResourceTypeEntry> {
public:
    ResourceTypeEntry()
        : ResourceTypeEntry("Root Resource", 0, "root")
    {}

    ResourceTypeEntry( const std::string& _name, uint32_t index, const std::string& _key)
        : BaseTypeEntry(_name, index, _key)
        , mass_(1000.0)
        , storage_(StorageType::BULK)
        , units_(Units::KILOGRAMS_MASS)
        , volume_(1.0)
    {}

    ~ResourceTypeEntry() = default;

    static std::string entry_type_name(){ return "ResourceTypeEntry";}

    float mass() const { return mass_; }

    StorageType storage() const { return storage_; }

    std::string str() const;

    void update( const nlohmann::json& blob );

    void update( const ResourceTypeEntry& parent );

    bool valid() const;

    float volume() const { return volume_; }

    Units& units() { return units_; }


public:
    const static ResourceTypeEntry RootResource;

    float mass_; // units = kilograms
    StorageType storage_;
    Units units_;
    float volume_;  // units == L (liters, litres)

};

}  // namespace Catalog
