// Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>

namespace Catalog {

class MobilityType {
public:
    enum MOBILITY_TYPE_ENUM {
      UNKNOWN =    '?',
      STATIC =     'S',
      INFANTRY =   'I',
      WHEELED =    'W',
      TRACKED =    'T',
      RAIL =       'R',
      MECH =       'M',
      HELICOPTER = 'H',
      AIRPLANE =   'A',
      BOAT =       'B',
      SHIP =       'O',
      SUBMARINE =  'U'
    };
    MOBILITY_TYPE_ENUM value;

public:
    MobilityType() = delete;

    MobilityType( MOBILITY_TYPE_ENUM e ): value(e) {}

    MobilityType( const MobilityType& other ): value(other.value) {}

    char code() const { return value; }

    std::string describe() const;

    void operator=( MobilityType other ){  value = other.value; }

    bool operator==( const MobilityType& rhs ) const { return value == rhs.value; }

    bool operator==( const MOBILITY_TYPE_ENUM other_value ) const { return value == other_value; }

    std::string name() const; 

    static MobilityType parse( const std::string& text );
};

}  // namespace Catalog
