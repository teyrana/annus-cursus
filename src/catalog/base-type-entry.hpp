//Copyright (c) 2023, MIT License, Daniel Williams
#pragma once

#include <string>
#include <sstream>

#include "tag-set.hpp"

  
namespace Catalog {

template< typename entry_type_t >
class BaseTypeEntry {
public:

    BaseTypeEntry( const std::string& _name, uint32_t i, const std::string& _key )
        : index(i)
        , key(_key)
        , name(_name)
    {}
    
    std::string str(){
        return static_cast<entry_type_t*>(this)->str(); }

    bool valid() { 
        return static_cast<entry_type_t*>(this)->valid(); }

    static std::string entry_type_name(){ return entry_type_t::entry_type_name(); }

protected:
    std::string header_string() const {
        std::ostringstream buf;
        buf << "- [" << index << "][" << key << "]: \"" << name << "\"";
        return buf.str();
    }

public:
    std::string description;
    const uint32_t index;
    const std::string key;
    const std::string name;
    std::string super;
    TagSet tags;


};

}  // namespace Catalog

