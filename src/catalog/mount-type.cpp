// Copyright (c) 2023, MIT License, Daniel Williams

#include <algorithm>
#include <string>

#include "mount-type.hpp"

using Catalog::MountType;

std::string MountType::describe() const {
    switch(value){
        default:
        case MountType::INTERNAL:
            return "Internal";
        case MountType::HAND:
            return "Hand";
        case MountType::STATIONARY:
            return "Stationary";
        case MountType::FIXED:
            return "Fixed";
    }
}

std::string MountType::name() const {
    switch(value){
        default:
        case MountType::INTERNAL:
            return "Internal";
        case MountType::HAND:
            return "Hand";
        case MountType::STATIONARY:
            return "Stationary";
        case MountType::FIXED:
            return "Fixed";
    }
}

MountType MountType::parse( const std::string& text ){
    if( 0 == text.size() ){
        return MountType::INTERNAL;
    }else if( 1 == text.size() ){
        switch(tolower(text[0])){
            default:
            case MountType::INTERNAL:
                return MountType::INTERNAL;
            case MountType::HAND:
                return MountType::HAND;
            case MountType::STATIONARY:
                return MountType::STATIONARY;
            case MountType::FIXED:
                return MountType::FIXED;
        }
    }

    // convert parameter string to lower case
    std::string lowkey = text;
    std::transform(lowkey.begin(), lowkey.end(), lowkey.begin(), ::tolower);

    if( lowkey == "internal" ){
        return MountType::INTERNAL;
    }else if( lowkey == "hand" ){
        return MountType::HAND;
    }else if( lowkey == "stationary" ){
        return MountType::STATIONARY;
    }else if( lowkey == "fixed" ){
        return MountType::FIXED;
    }

    return MountType::INTERNAL;
}



