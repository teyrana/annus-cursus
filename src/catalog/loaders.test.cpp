#include <iostream>
#include <cassert>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

// project imports
#include "index.hpp"
#include "building-type-entry.hpp"
#include "mobility-type.hpp"
#include "module-type-entry.hpp"
#include "platform-type-entry.hpp"
#include "process-type-entry.hpp"
#include "resource-type-entry.hpp"
#include "technology-type-entry.hpp"
#include "unit-type-entry.hpp"

using Catch::Matchers::WithinAbs;

using Catalog::Index;
using Catalog::BuildingTypeEntry;
using Catalog::MobilityType;
using Catalog::ModuleTypeEntry;
using Catalog::MountType;

using Catalog::StorageType;


TEST_CASE( "Can load data files correctly ", "[loading]" ){
    // load all the data files once, and then validate that it proceeded correctly
    Catalog::Index catalog;
    REQUIRE( catalog.load("data/catalog/") );

    // ("Can Load Buildings")
    {
        REQUIRE( 26 == catalog.building.size() );

        // not exhaustive; just some spot checks
        const auto* b1 = catalog.building.get("bulk_storage_1x1");
        REQUIRE( nullptr != b1 );
        REQUIRE( b1->name ==  "Small Storage Shed (1x1)" );
        REQUIRE( 10000 == b1->hitpoints() );
        REQUIRE_THAT( b1->armor(), WithinAbs(100, 0.1) );
        REQUIRE( b1->footprint().x == 10 );
        REQUIRE( b1->footprint().y == 10 );
        REQUIRE_THAT( b1->volume(), WithinAbs(100, 0.1) );
        REQUIRE( b1->mounts().size() == 1 );      
        REQUIRE( b1->mounts().at(0) == MountType::INTERNAL );
        REQUIRE( b1->modules().size() == 1 );
        REQUIRE( 0 < b1->modules().count("bulk_storage") );
    }
    
    //SECTION("Can Load Module Entries")
    {
        REQUIRE( 145 == catalog.module.size() );

        // not exhaustive; just some spot checks
        {
            const auto* bulk = catalog.module.get("bulk_storage");
            REQUIRE( nullptr != bulk );
            REQUIRE( bulk->name == "Bulk Storage");
            REQUIRE( 0 == bulk->tags.size() );
            REQUIRE( bulk->super.empty() );
            REQUIRE_THAT( bulk->mass(), WithinAbs(10000, 0.1) );
            REQUIRE_THAT( bulk->volume(), WithinAbs(1000, 0.1) );
            REQUIRE( 0 < bulk->storage().count( StorageType::BULK ) );
        } {
            const auto* o2gen = catalog.module.get("o2_generator");
            REQUIRE( nullptr != o2gen);
            REQUIRE( o2gen->name == "O2 Generator" );
            REQUIRE( o2gen->tags.contains("habitation"));
            REQUIRE( o2gen->tags.contains("life_support"));
            REQUIRE( o2gen->super.empty() );
            REQUIRE_THAT( o2gen->volume(), WithinAbs(250, 0.1) );
        }
    }

    //SECTION("Can load Platform Entries")
    {
        REQUIRE( 32 == catalog.platform.size() );

        // not exhaustive; just some spot checks
        {
            const auto* human = catalog.platform.get("human");
            REQUIRE( nullptr != human);
            REQUIRE( human->name == "Human");
            REQUIRE( human->super.empty() );
            REQUIRE( human->tags.size() == 0 );
            REQUIRE_THAT( human->mass(), WithinAbs(120, 0.1) );
            REQUIRE_THAT( human->volume(), WithinAbs(60, 0.1) ); 
            REQUIRE( human->mounts().size() == 1 );
            REQUIRE( human->mounts().at(MountType::HAND) == 2 );
            REQUIRE( human->mobility() == MobilityType::INFANTRY );
        } {
            const auto* sedan = catalog.platform.get("commercial_auto");
            REQUIRE( nullptr != sedan);
            REQUIRE( sedan->name == "Sedan" );
            REQUIRE( sedan->super == "4wheel" );
            REQUIRE( sedan->tags.size() == 0);
            REQUIRE( sedan->mass() == 500);
            REQUIRE( sedan->volume() == 120);
            REQUIRE_THAT( sedan->armor(), WithinAbs(10, 0.1) );
            REQUIRE( sedan->mounts().size() == 1 );
            REQUIRE( sedan->mounts().at(MountType::INTERNAL) == 1 );
            REQUIRE( sedan->mobility() == MobilityType::WHEELED );
        }
    }
    
    //SECTION("Can load Process Entries")
    {
        REQUIRE( 100 == catalog.process.size() );

        // not exhaustive; just some spot checks
        {
            const auto* burn = catalog.process.get("burn_biomass_1mw");
            REQUIRE( nullptr != burn);
            REQUIRE( burn->name == "Burn Biomass" );
            REQUIRE( burn->tags.contains("combustion") );
            REQUIRE( burn->super.empty() );
            REQUIRE( 0 < burn->io().count("biomass") );
            REQUIRE( 0 < burn->io().count("co2") );
            REQUIRE( 0 < burn->io().count("oxygen") );
            REQUIRE( 0 < burn->io().count("heat") );
        }
    }

    //SECTION("Can load Resource Entries")
    {
        REQUIRE( 219 == catalog.resource.size() );

        // not exhaustive; just some spot checks
        {
            const auto* oxy = catalog.resource.get("oxygen");
            REQUIRE( nullptr != oxy);
            REQUIRE( oxy->name == "Oxygen" );
            REQUIRE( oxy->tags.contains("air"));
            REQUIRE( oxy->super == "gas");
            REQUIRE( oxy->storage() == StorageType::GAS );
        } {
            const auto* tiberium = catalog.resource.get("tiberium");
            REQUIRE( nullptr != tiberium );
            REQUIRE( tiberium->name == "Tiberium" );
            REQUIRE( tiberium->tags.contains("tiberium") );
            REQUIRE( tiberium->super.empty() );
            REQUIRE_THAT( tiberium->volume(), WithinAbs(0.5, 0.1) );
            REQUIRE_THAT( tiberium->mass(), WithinAbs(1000, 0.1) );
            REQUIRE( tiberium->storage() == StorageType::BULK );
        } {
            const auto* blue_tiberium = catalog.resource.get("blue_tiberium");
            REQUIRE( nullptr != blue_tiberium );
            REQUIRE( blue_tiberium->name == "Blue Tiberium" );
            REQUIRE_THAT( blue_tiberium->volume(), WithinAbs(0.5, 0.1) );
            REQUIRE_THAT( blue_tiberium->mass(), WithinAbs(1000, 0.1) );
            REQUIRE( blue_tiberium->super == "tiberium" );
            REQUIRE( blue_tiberium->tags.contains("tiberium") );
            REQUIRE( blue_tiberium->storage() == StorageType::BULK );
        }{
            const auto* red_tiberium = catalog.resource.get("red_tiberium");
            REQUIRE( nullptr != red_tiberium );
            REQUIRE( red_tiberium->name == "Red Tiberium" );
            REQUIRE( red_tiberium->tags.contains("tiberium") );
            REQUIRE( red_tiberium->super == "tiberium" );
            REQUIRE_THAT( red_tiberium->volume(), WithinAbs(0.5, 0.1) );
            REQUIRE_THAT( red_tiberium->mass(), WithinAbs(1000, 0.1) );
            REQUIRE( red_tiberium->storage() == StorageType::BULK );
        }{
            const auto* sheep = catalog.resource.get("sheep");
            REQUIRE( nullptr != sheep);
            REQUIRE( sheep->name == "Live Sheep" );
            REQUIRE_THAT( sheep->volume(), WithinAbs(400, 0.1) );
            REQUIRE_THAT( sheep->mass(), WithinAbs(160, 0.1) );
            REQUIRE( sheep->super == "livestock" );
            REQUIRE( sheep->storage() == StorageType::LIVESTOCK );
        }
    }

    //SECTION("Can load Technology Entries")
    {
        REQUIRE( 51 == catalog.technology.size() );

        // not exhaustive; just some spot checks
        {
            const auto* smelt_gold = catalog.technology.get("smelt_gold");
            REQUIRE( nullptr != smelt_gold );
            REQUIRE( smelt_gold->name == "Smelt Gold" );
            REQUIRE( smelt_gold->tags.size() == 0 );
            REQUIRE( smelt_gold->super.empty() );
            REQUIRE( smelt_gold->requirements().size() == 0 );
        }{
            const auto* cast_iron = catalog.technology.get("cast_iron");
            REQUIRE( nullptr != cast_iron);
            REQUIRE( "Cast Iron" == cast_iron->name );
            REQUIRE( cast_iron->tags.contains("steel") );
            REQUIRE( 0 < cast_iron->requirements().count("smelt_iron"));
        }
    }

    //SECTION("Can load Unit Entries")
    {
      REQUIRE( 50 == catalog.unit.size() );
      {
          const auto* conscript = catalog.unit.get("conscript");
          REQUIRE( nullptr != conscript );
          REQUIRE( conscript->name == "Conscript Soldier" );
          REQUIRE( conscript->tags.size() == 0 );
          REQUIRE( conscript->super == "person" );
          REQUIRE( conscript->modules().size() == 0 );
          REQUIRE( conscript->platform() == "human" );
      }
  }

}