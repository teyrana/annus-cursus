BUILD_TYPE="debug"
SHELL:=bash
SHELL:=bash
CONTAINERD=podman

EXE=build/src/ancursus

# local paths (host system)
GIT_ROOT=$(shell git rev-parse --show-toplevel)
PROJECT_ROOT=${GIT_ROOT}

# container parameters (container / client system)

DOCKERFILE_FEDORA=${PROJECT_ROOT}/.devcontainer/dockerfile.fedora
DOCKERFILE_UBUNTU=${PROJECT_ROOT}/.devcontainer/dockerfile.ubuntu
CONTAINER_MOUNT_PATH=/workspaces/annus-cursus
DOCKERFILE_PATH=${DOCKERFILE_UBUNTU}
CONTAINER_IMAGE_LABEL=devel
CONTAINER_NAME=annus-cursus-vscode-dev

CONTAINER_REGISTRY_URL=registry.gitlab.com
# works on for my (gitlab) account -- other users will probably have to change / workaround
REGISTRY_USER=${USER}
REGISTRY_REPO=annus-cursus

CONTAINER_IMAGE_TAG=${CONTAINER_REGISTRY_URL}/${REGISTRY_USER}/${REGISTRY_REPO}


# -------------------- Define Targets -------------------------------

# default target goes first
.PHONY: default
default: build-all


# Docker Targets
# --------------

.PHONY: attach
attach: container-exec

container-build: container-build-ubuntu

container-build-fedora:
	${CONTAINERD} build --tag=${CONTAINER_IMAGE_TAG}:fedora --tag=${CONTAINER_IMAGE_TAG}:latest --file=${DOCKERFILE_FEDORA} ${PROJECT_ROOT}

container-build-ubuntu:
	${CONTAINERD} build --tag=${CONTAINER_IMAGE_TAG}:ubuntu --tag=${CONTAINER_IMAGE_TAG}:latest --file=${DOCKERFILE_UBUNTU} ${PROJECT_ROOT}

container-exec:
	@ ${CONTAINERD} exec -it -w ${CONTAINER_MOUNT_PATH} ${CONTAINER_NAME} ${SHELL}

# Pushing the CI image may require a (gitlab) registry login setup:
# --> this will involve out-of-band setup of your particular gitlab account + docker/podman login
# --> these tokens are not included in the repo
# echo "::CONTAINER_REGISTRY_URL:  ${CONTAINER_REGISTRY_URL}"
# echo "::CONTAINER_IMAGE_TAG:     ${CONTAINER_IMAGE_TAG}"
container-push-ci:
	${CONTAINERD} login ${CONTAINER_REGISTRY_URL}
	${CONTAINERD} push ${CONTAINER_IMAGE_TAG}:latest
	${CONTAINERD} push ${CONTAINER_IMAGE_TAG}:fedora
	${CONTAINERD} push ${CONTAINER_IMAGE_TAG}:ubuntu

container-run: container-run-ubuntu

container-run-fedora: MOUNT_ARGS=-v${IMAGE_CONTEXT_PATH}:${CONTAINER_MOUNT_PATH}
container-run-fedora: #container-build-fedora
	${CONTAINERD} run -it --rm -w ${CONTAINER_MOUNT_PATH} --name=${CONTAINER_NAME} ${MOUNT_ARGS} ${CONTAINER_IMAGE_TAG}:fedora

container-run-ubuntu: MOUNT_ARGS=-v${IMAGE_CONTEXT_PATH}:${CONTAINER_MOUNT_PATH}
container-run-ubuntu: #container-build-ubuntu
	${CONTAINERD} run -it --rm -w ${CONTAINER_MOUNT_PATH} --name=${CONTAINER_NAME} ${MOUNT_ARGS} ${CONTAINER_IMAGE_TAG}:ubuntu



# Core Targets -- build the libraries and binaries themselves
# --------------
.PHONY: build
build: build-all
build-all: build/CMakeCache.txt
	cd build && ninja

.PHONY: config, configure
config:configure
build/CMakeCache.txt: configure
configure:
	mkdir -p build
	cd build && cmake -GNinja -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..

run: build
	${EXE} -l saves/dev.save.json

test: build
	build/src/catalog/catalog-tests

# tiles: map
# map: build
# 	${EXE} -l dev.json


# view: debug.png
# 	eog debug.png

clean: 
	rm -rf build/*
	rm -f .DS_Store

