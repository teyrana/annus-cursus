Procedural Map Generation Tool
-------

This code is a hobby project to experiment with map generation.

This tool is so far experimental, and is not (yet) compatible with anything.


Output Formats
====
Terrain is generated from scratch, and may be output in:
1. Bitmap (PNG)
2. Text (to terminal)
3. Tiled JSON format (NYI)


Implemented Terrain Effects:
====
Effects are apply in a specified order, as specified in configuration file.
1. [x] Cap - remove all material above a given height
2. [x] Fill - add material to any cell below a given height
3. [x] Simplex Hills - Generate hills from simplex noise
4. [x] Slope - Add a uniform slope
5. [x] Step - flatten the given terrain, in a step-like pattern
6. [x] Craters (Bombardment) - Generate craters to the given terrain
7. [x] Voronoi Hills - Generate hills from Voronoi adjacency noise


Planned Effects:
=================
1. Roads
1. Towns + Roads
1. Rivers
1. White Noise effect
1. erosion (?)


Dependencies
============

## External 'apt' Packages  (@Ubuntu 24.04)
1. 'libpng-dev'
2. 'catch2'
3. 'libcxxopts-dev'
4. 'nlohmann-json3-dev'

        
## (inactive) External 'extern/' Libraries
# --?-- 'simplexnoise': (C) Eliot Eshelman - GPLv3 License

# License
=========
Project-Internal Code is MIT-Licensed
